@echo off

REM Vars
set "SLNDIR=%~dp0"

REM Restore + Build
dotnet build "%SLNDIR%\src\Aaravos.Compiler" --nologo || exit /b

REM Run
dotnet run -p "%SLNDIR%\src\Aaravos.Compiler" --no-build -- %*
