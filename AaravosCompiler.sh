#!/bin/bash

# Vars
slndir="$(dirname "${BASH_SOURCE[0]}")"

# Restore + Build
dotnet build "$slndir/src/Aaravos.Compiler" --nologo || exit

# Run
dotnet run -p "$slndir/src/Aaravos.Compiler" --no-build -- "$@"
