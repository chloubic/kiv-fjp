@echo off

REM Vars
set "SLNDIR=%~dp0"

REM Restore + Build
dotnet build "%SLNDIR%\src\Aaravos.REPL" --nologo || exit /b

REM Run
dotnet run -p "%SLNDIR%\src\Aaravos.REPL" --no-build
