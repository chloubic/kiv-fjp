@echo off

REM Vars
set "SLNDIR=%~dp0"

REM Restore + Build
dotnet build "%SLNDIR%\Aaravos.sln" --nologo || exit /b

REM Test
dotnet test "%SLNDIR%\src\Aaravos.Tests" --nologo --no-build
