#!/bin/bash

# Vars
slndir="$(dirname "${BASH_SOURCE[0]}")"

# Restore + Build
dotnet build "$slndir/Aaravos.sln" --nologo || exit

# Test
dotnet test "$slndir/src/Arravos.Tests" --nologo --no-build
