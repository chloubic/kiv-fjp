using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Facts
{
    public static class SyntaxFacts
    {

        #region Methods

        /// <summary>
        /// Resolves precedence of unary operators
        /// </summary>
        /// <param name="kind">Unary operator kind</param>
        /// <returns>Precedence priority</returns>
        public static int GetUnaryOperatorPrecedence(this SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.PlusToken:
                case SyntaxKind.MinusToken:
                case SyntaxKind.BangToken:
                case SyntaxKind.TildeToken:
                    return 6;

                default:
                    return 0;
            }
        }

        /// <summary>
        /// Resolves precedence of binary operators
        /// </summary>
        /// <param name="kind">Binary operator kind</param>
        /// <returns>Precedence priority</returns>
        public static int GetBinaryOperatorPrecedence(this SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.StarToken:
                case SyntaxKind.SlashToken:
                    return 5;

                case SyntaxKind.PlusToken:
                case SyntaxKind.MinusToken:
                    return 4;

                case SyntaxKind.EqualsEqualsToken:
                case SyntaxKind.BangEqualsToken:
                case SyntaxKind.LessToken:
                case SyntaxKind.LessOrEqualsToken:
                case SyntaxKind.GreaterToken:
                case SyntaxKind.GreaterOrEqualsToken:
                    return 3;

                case SyntaxKind.AmpersandToken:
                case SyntaxKind.AmpersandAmpersandToken:
                    return 2;

                case SyntaxKind.PipeToken:
                case SyntaxKind.PipePipeToken:
                case SyntaxKind.HatToken:
                    return 1;

                default:
                    return 0;
            }
        }

        /// <summary>
        /// Checks if given kind is comment
        /// </summary>
        /// <param name="kind">Given kind</param>
        /// <returns>If kind is comment</returns>
        public static bool IsComment(this SyntaxKind kind)
            => kind == SyntaxKind.SingleLineCommentTrivia ||
                   kind == SyntaxKind.MultiLineCommentTrivia;

        /// <summary>
        /// Returns king of given text
        /// </summary>
        /// <param name="text">Given text</param>
        /// <returns>Kind of given text</returns>
        public static SyntaxKind GetKeywordKind(string text)
        {
            switch (text)
            {
                case KeywordConstants.BREAK:
                    return SyntaxKind.BreakKeyword;
                case KeywordConstants.CONTINUE:
                    return SyntaxKind.ContinueKeyword;
                case KeywordConstants.ELSE:
                    return SyntaxKind.ElseKeyword;
                case KeywordConstants.FALSE:
                    return SyntaxKind.FalseKeyword;
                case KeywordConstants.FOR:
                    return SyntaxKind.ForKeyword;
                case KeywordConstants.FUNCTION:
                    return SyntaxKind.FunctionKeyword;
                case KeywordConstants.IF:
                    return SyntaxKind.IfKeyword;
                case KeywordConstants.CONST:
                    return SyntaxKind.ConstKeyword;
                case KeywordConstants.RETURN:
                    return SyntaxKind.ReturnKeyword;
                case KeywordConstants.TO:
                    return SyntaxKind.ToKeyword;
                case KeywordConstants.TRUE:
                    return SyntaxKind.TrueKeyword;
                case KeywordConstants.VAR:
                    return SyntaxKind.VarKeyword;
                case KeywordConstants.WHILE:
                    return SyntaxKind.WhileKeyword;
                case KeywordConstants.DO:
                    return SyntaxKind.DoKeyword;
                default:
                    return SyntaxKind.IdentifierToken;
            }
        }

        public static IEnumerable<SyntaxKind> GetUnaryOperatorKinds()
        {
            var kinds = (SyntaxKind[])Enum.GetValues(typeof(SyntaxKind));
            foreach (var kind in kinds)
            {
                if (GetUnaryOperatorPrecedence(kind) > 0)
                    yield return kind;
            }
        }

        public static IEnumerable<SyntaxKind> GetBinaryOperatorKinds()
        {
            var kinds = (SyntaxKind[])Enum.GetValues(typeof(SyntaxKind));
            foreach (var kind in kinds)
            {
                if (GetBinaryOperatorPrecedence(kind) > 0)
                    yield return kind;
            }
        }

        /// <summary>
        /// Returns text of given kind
        /// </summary>
        /// <param name="kind">Given king</param>
        /// <returns>Text of given kind</returns>
        public static string? GetText(SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.PlusToken:
                    return TokenConstants.PLUS.ToString();
                case SyntaxKind.PlusEqualsToken:
                    return TokenConstants.PLUS_EQUALS;
                case SyntaxKind.MinusToken:
                    return TokenConstants.MINUS.ToString();
                case SyntaxKind.MinusEqualsToken:
                    return TokenConstants.MINUS_EQUALS;
                case SyntaxKind.StarToken:
                    return TokenConstants.STAR.ToString();
                case SyntaxKind.StarEqualsToken:
                    return TokenConstants.STAR_EQUALS;
                case SyntaxKind.SlashToken:
                    return TokenConstants.SLASH.ToString();
                case SyntaxKind.SlashEqualsToken:
                    return TokenConstants.SLASH_EQUALS;
                case SyntaxKind.BangToken:
                    return TokenConstants.EXCLAMATION_MARK.ToString();
                case SyntaxKind.EqualsToken:
                    return TokenConstants.EQUALITY.ToString();
                case SyntaxKind.TildeToken:
                    return TokenConstants.TILDE.ToString();
                case SyntaxKind.LessToken:
                    return TokenConstants.LESS_BRACKET.ToString();
                case SyntaxKind.LessOrEqualsToken:
                    return TokenConstants.LESS_EQUALS;
                case SyntaxKind.GreaterToken:
                    return TokenConstants.GREATER_BRACKET.ToString();
                case SyntaxKind.GreaterOrEqualsToken:
                    return TokenConstants.GREATER_EQUALS;
                case SyntaxKind.AmpersandToken:
                    return TokenConstants.AMPERSAND.ToString();
                case SyntaxKind.AmpersandAmpersandToken:
                    return TokenConstants.LOGICAL_AND;
                case SyntaxKind.AmpersandEqualsToken:
                    return TokenConstants.LOGICAL_AND_EQUALS;
                case SyntaxKind.PipeToken:
                    return TokenConstants.PIPE.ToString();
                case SyntaxKind.PipeEqualsToken:
                    return TokenConstants.LOGICAL_OR_EQUALS;
                case SyntaxKind.PipePipeToken:
                    return TokenConstants.LOGICAL_OR;
                case SyntaxKind.HatToken:
                    return TokenConstants.HAT.ToString();
                case SyntaxKind.HatEqualsToken:
                    return TokenConstants.HAT_EQUALS;
                case SyntaxKind.EqualsEqualsToken:
                    return TokenConstants.DOUBLE_EQUALS;
                case SyntaxKind.BangEqualsToken:
                    return TokenConstants.NOT_EQUALS;
                case SyntaxKind.OpenParenthesisToken:
                    return TokenConstants.OPEN_ROUND_BRACKET.ToString();
                case SyntaxKind.CloseParenthesisToken:
                    return TokenConstants.CLOSE_ROUND_BRACKET.ToString();
                case SyntaxKind.OpenBraceToken:
                    return TokenConstants.OPEN_CURLY_BRACKET.ToString();
                case SyntaxKind.CloseBraceToken:
                    return TokenConstants.CLOSE_CURLY_BRACKET.ToString();
                case SyntaxKind.ColonToken:
                    return TokenConstants.COLON.ToString();
                case SyntaxKind.CommaToken:
                    return TokenConstants.COMMA.ToString();
                case SyntaxKind.BreakKeyword:
                    return KeywordConstants.BREAK;
                case SyntaxKind.ContinueKeyword:
                    return KeywordConstants.CONTINUE;
                case SyntaxKind.ElseKeyword:
                    return KeywordConstants.ELSE;
                case SyntaxKind.FalseKeyword:
                    return KeywordConstants.FALSE;
                case SyntaxKind.ForKeyword:
                    return KeywordConstants.FOR;
                case SyntaxKind.FunctionKeyword:
                    return KeywordConstants.FUNCTION;
                case SyntaxKind.IfKeyword:
                    return KeywordConstants.IF;
                case SyntaxKind.ConstKeyword:
                    return KeywordConstants.CONST;
                case SyntaxKind.ReturnKeyword:
                    return KeywordConstants.RETURN;
                case SyntaxKind.ToKeyword:
                    return KeywordConstants.TO;
                case SyntaxKind.TrueKeyword:
                    return KeywordConstants.TRUE;
                case SyntaxKind.VarKeyword:
                    return KeywordConstants.VAR;
                case SyntaxKind.WhileKeyword:
                    return KeywordConstants.WHILE;
                case SyntaxKind.DoKeyword:
                    return KeywordConstants.DO;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Checks if given king is trivia
        /// </summary>
        /// <param name="kind">Given syntax kind</param>
        /// <returns>If given king is trivia</returns>
        public static bool IsTrivia(this SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.SkippedTextTrivia:
                case SyntaxKind.LineBreakTrivia:
                case SyntaxKind.WhitespaceTrivia:
                case SyntaxKind.SingleLineCommentTrivia:
                case SyntaxKind.MultiLineCommentTrivia:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Checks if given king is keyword
        /// </summary>
        /// <param name="kind">Given syntax kind</param>
        /// <returns>If given king is keyword</returns>
        public static bool IsKeyword(this SyntaxKind kind)
            => kind.ToString().EndsWith("Keyword");

        /// <summary>
        /// Checks if given king is token
        /// </summary>
        /// <param name="kind">Given syntax kind</param>
        /// <returns>If given king is token</returns>
        public static bool IsToken(this SyntaxKind kind)
            => !kind.IsTrivia() &&
                   (kind.IsKeyword() || kind.ToString().EndsWith("Token"));

        /// <summary>
        /// Returns binary operator of assignment operator
        /// </summary>
        /// <param name="kind">Assignment operator king</param>
        /// <returns>Binary operator of assignment operator</returns>
        public static SyntaxKind GetBinaryOperatorOfAssignmentOperator(SyntaxKind kind)
        {
            switch (kind)
            {
                case SyntaxKind.PlusEqualsToken:
                    return SyntaxKind.PlusToken;
                case SyntaxKind.MinusEqualsToken:
                    return SyntaxKind.MinusToken;
                case SyntaxKind.StarEqualsToken:
                    return SyntaxKind.StarToken;
                case SyntaxKind.SlashEqualsToken:
                    return SyntaxKind.SlashToken;
                case SyntaxKind.AmpersandEqualsToken:
                    return SyntaxKind.AmpersandToken;
                case SyntaxKind.PipeEqualsToken:
                    return SyntaxKind.PipeToken;
                case SyntaxKind.HatEqualsToken:
                    return SyntaxKind.HatToken;
                default:
                    throw new Exception($"Unexpected syntax: '{kind}'");
            }
        }

        #endregion Methods

    }
}
