using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Kinds
{
    public enum SymbolKind
    {
        Function,
        GlobalVariable,
        LocalVariable,
        Parameter,
        Type,
    }
}
