using Aaravos.Core.CodeAnalysis.Analyzing.Facts;
using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Constants;
using Aaravos.Core.CodeAnalysis.Diagnosting;
using Aaravos.Core.CodeAnalysis.Texting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing
{
    internal sealed class Lexer
    {

        #region Properties

        private readonly DiagnosticBag DiagnosticsProperty = new DiagnosticBag();
        private readonly SyntaxTree SyntaxTree;
        private readonly SourceText Text;
        private int Position;

        private int Start;
        private SyntaxKind Kind;
        private object? Value;
        private ImmutableArray<SyntaxTrivia>.Builder TriviaBuilder = ImmutableArray.CreateBuilder<SyntaxTrivia>();

        public DiagnosticBag Diagnostics => DiagnosticsProperty;

        private char Current => Peek(0);

        private char Lookahead => Peek(1);

        #endregion Properties

        #region Constructor

        public Lexer(
            SyntaxTree syntaxTree
        )
        {
            SyntaxTree = syntaxTree;
            Text = syntaxTree.Text;
        }

        #endregion Constructor

        #region Methods

        private char Peek(int offset)
        {
            var index = Position + offset;

            if (index >= Text.Length)
                return '\0';

            return Text[index];
        }

        public SyntaxToken Lex()
        {
            ReadTrivia(leading: true);

            var leadingTrivia = TriviaBuilder.ToImmutable();
            var tokenStart = Position;

            ReadToken();

            var tokenKind = Kind;
            var tokenValue = Value;
            var tokenLength = Position - Start;

            ReadTrivia(leading: false);

            var trailingTrivia = TriviaBuilder.ToImmutable();

            var tokenText = SyntaxFacts.GetText(tokenKind);
            if (tokenText == null)
                tokenText = Text.ToString(tokenStart, tokenLength);

            return new SyntaxToken(SyntaxTree, tokenKind, tokenStart, tokenText, tokenValue, leadingTrivia, trailingTrivia);
        }

        private void ReadTrivia(bool leading)
        {
            TriviaBuilder.Clear();

            var done = false;

            while (!done)
            {
                Start = Position;
                Kind = SyntaxKind.BadToken;
                Value = null;

                switch (Current)
                {
                    case '\0':
                        done = true;
                        break;
                    case '/':
                        if (Lookahead == '/')
                        {
                            ReadSingleLineComment();
                        }
                        else if (Lookahead == '*')
                        {
                            ReadMultiLineComment();
                        }
                        else
                        {
                            done = true;
                        }
                        break;
                    case '\n':
                    case '\r':
                        if (!leading)
                            done = true;
                        ReadLineBreak();
                        break;
                    case ' ':
                    case '\t':
                        ReadWhiteSpace();
                        break;
                    default:
                        if (char.IsWhiteSpace(Current))
                            ReadWhiteSpace();
                        else
                            done = true;
                        break;
                }

                var length = Position - Start;
                if (length > 0)
                {
                    var text = Text.ToString(Start, length);
                    var trivia = new SyntaxTrivia(SyntaxTree, Kind, Start, text);
                    TriviaBuilder.Add(trivia);
                }
            }
        }

        private void ReadLineBreak()
        {
            if (Current == '\r' && Lookahead == '\n')
            {
                Position += 2;
            }
            else
            {
                Position++;
            }

            Kind = SyntaxKind.LineBreakTrivia;
        }

        private void ReadWhiteSpace()
        {
            var done = false;

            while (!done)
            {
                switch (Current)
                {
                    case '\0':
                    case '\r':
                    case '\n':
                        done = true;
                        break;
                    default:
                        if (!char.IsWhiteSpace(Current))
                            done = true;
                        else
                            Position++;
                        break;
                }
            }

            Kind = SyntaxKind.WhitespaceTrivia;
        }


        private void ReadSingleLineComment()
        {
            Position += 2;
            var done = false;

            while (!done)
            {
                switch (Current)
                {
                    case '\0':
                    case '\r':
                    case '\n':
                        done = true;
                        break;
                    default:
                        Position++;
                        break;
                }
            }

            Kind = SyntaxKind.SingleLineCommentTrivia;
        }

        private void ReadMultiLineComment()
        {
            Position += 2;
            var done = false;

            while (!done)
            {
                switch (Current)
                {
                    case TokenConstants.END_OF_STRING:
                        var span = new TextSpan(Start, 2);
                        var location = new TextLocation(Text, span);
                        DiagnosticsProperty.ReportUnterminatedMultiLineComment(location);
                        done = true;
                        break;
                    case TokenConstants.STAR:
                        if (Lookahead == TokenConstants.SLASH)
                        {
                            Position++;
                            done = true;
                        }
                        Position++;
                        break;
                    default:
                        Position++;
                        break;
                }
            }

            Kind = SyntaxKind.MultiLineCommentTrivia;
        }

        private void ReadToken()
        {
            Start = Position;
            Kind = SyntaxKind.BadToken;
            Value = null;

            switch (Current)
            {
                case TokenConstants.END_OF_STRING:
                    Kind = SyntaxKind.EndOfFileToken;
                    break;
                case TokenConstants.PLUS:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.PlusToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.PlusEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.MINUS:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.MinusToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.MinusEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.STAR:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.StarToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.StarEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.SLASH:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.SlashToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.SlashEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.OPEN_ROUND_BRACKET:
                    Kind = SyntaxKind.OpenParenthesisToken;
                    Position++;
                    break;
                case TokenConstants.CLOSE_ROUND_BRACKET:
                    Kind = SyntaxKind.CloseParenthesisToken;
                    Position++;
                    break;
                case TokenConstants.OPEN_CURLY_BRACKET:
                    Kind = SyntaxKind.OpenBraceToken;
                    Position++;
                    break;
                case TokenConstants.CLOSE_CURLY_BRACKET:
                    Kind = SyntaxKind.CloseBraceToken;
                    Position++;
                    break;
                case TokenConstants.COLON:
                    Kind = SyntaxKind.ColonToken;
                    Position++;
                    break;
                case TokenConstants.COMMA:
                    Kind = SyntaxKind.CommaToken;
                    Position++;
                    break;
                case TokenConstants.TILDE:
                    Kind = SyntaxKind.TildeToken;
                    Position++;
                    break;
                case TokenConstants.HAT:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.HatToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.HatEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.AMPERSAND:
                    Position++;
                    if (Current == TokenConstants.AMPERSAND)
                    {
                        Kind = SyntaxKind.AmpersandAmpersandToken;
                        Position++;
                    }
                    else if (Current == TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.AmpersandEqualsToken;
                        Position++;
                    }
                    else
                    {
                        Kind = SyntaxKind.AmpersandToken;
                    }
                    break;
                case TokenConstants.PIPE:
                    Position++;
                    if (Current == TokenConstants.PIPE)
                    {
                        Kind = SyntaxKind.PipePipeToken;
                        Position++;
                    }
                    else if (Current == TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.PipeEqualsToken;
                        Position++;
                    }
                    else
                    {
                        Kind = SyntaxKind.PipeToken;
                    }
                    break;
                case TokenConstants.EQUALITY:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.EqualsToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.EqualsEqualsToken;
                        Position++;
                    }
                    break;
                case TokenConstants.EXCLAMATION_MARK:
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.BangToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.BangEqualsToken;
                        Position++;
                    }
                    break;
                case '<':
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.LessToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.LessOrEqualsToken;
                        Position++;
                    }
                    break;
                case '>':
                    Position++;
                    if (Current != TokenConstants.EQUALITY)
                    {
                        Kind = SyntaxKind.GreaterToken;
                    }
                    else
                    {
                        Kind = SyntaxKind.GreaterOrEqualsToken;
                        Position++;
                    }
                    break;
                case '"':
                    ReadString();
                    break;
                case TokenConstants.NUMBER_0:
                case TokenConstants.NUMBER_1:
                case TokenConstants.NUMBER_2:
                case TokenConstants.NUMBER_3:
                case TokenConstants.NUMBER_4:
                case TokenConstants.NUMBER_5:
                case TokenConstants.NUMBER_6:
                case TokenConstants.NUMBER_7:
                case TokenConstants.NUMBER_8:
                case TokenConstants.NUMBER_9:
                    ReadNumber();
                    break;
                case TokenConstants.UNDERSCORE:
                    ReadIdentifierOrKeyword();
                    break;
                default:
                    if (char.IsLetter(Current))
                    {
                        ReadIdentifierOrKeyword();
                    }
                    else
                    {
                        var span = new TextSpan(Position, 1);
                        var location = new TextLocation(Text, span);
                        DiagnosticsProperty.ReportBadCharacter(location, Current);
                        Position++;
                    }
                    break;
            }
        }

        private void ReadString()
        {
            // Skip the current quote
            Position++;

            var sb = new StringBuilder();
            var done = false;

            while (!done)
            {
                switch (Current)
                {
                    case TokenConstants.END_OF_LINE_R:
                    case TokenConstants.END_OF_STRING:
                    case TokenConstants.END_OF_LINE_N:
                        var span = new TextSpan(Start, 1);
                        var location = new TextLocation(Text, span);
                        DiagnosticsProperty.ReportUnterminatedString(location);
                        done = true;
                        break;
                    case '"':
                        if (Lookahead == '"')
                        {
                            sb.Append(Current);
                            Position += 2;
                        }
                        else
                        {
                            Position++;
                            done = true;
                        }
                        break;
                    default:
                        sb.Append(Current);
                        Position++;
                        break;
                }
            }

            Kind = SyntaxKind.StringToken;
            Value = sb.ToString();
        }

        private void ReadNumber()
        {
            while (char.IsDigit(Current))
                Position++;

            var length = Position - Start;
            var text = Text.ToString(Start, length);
            if (!int.TryParse(text, out var value))
            {
                var span = new TextSpan(Start, length);
                var location = new TextLocation(Text, span);
                DiagnosticsProperty.ReportInvalidNumber(location, text, TypeSymbol.Int);
            }

            Value = value;
            Kind = SyntaxKind.NumberToken;
        }

        private void ReadIdentifierOrKeyword()
        {
            while (char.IsLetterOrDigit(Current) || Current == TokenConstants.UNDERSCORE)
                Position++;

            var length = Position - Start;
            var text = Text.ToString(Start, length);
            Kind = SyntaxFacts.GetKeywordKind(text);
        }

        #endregion Methods

    }
}
