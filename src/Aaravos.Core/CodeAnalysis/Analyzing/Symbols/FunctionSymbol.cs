using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public sealed class FunctionSymbol : Symbol
    {

        #region Properties

        public override SymbolKind Kind => SymbolKind.Function;
        public FunctionDeclarationSyntax? Declaration { get; }
        public ImmutableArray<ParameterSymbol> Parameters { get; }
        public TypeSymbol Type { get; }

        #endregion Properties

        #region Constructor

        internal FunctionSymbol(
            string name,
            ImmutableArray<ParameterSymbol> parameters,
            TypeSymbol type,
            FunctionDeclarationSyntax? declaration = null
        ) : base(name)
        {
            Parameters = parameters;
            Type = type;
            Declaration = declaration;
        }

        #endregion Constructor

    }
}
