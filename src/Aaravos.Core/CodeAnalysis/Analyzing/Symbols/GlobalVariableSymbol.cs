using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Binding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public sealed class GlobalVariableSymbol : VariableSymbol
    {

        #region Properties

        public override SymbolKind Kind => SymbolKind.GlobalVariable;

        #endregion Properties

        #region Constructor

        internal GlobalVariableSymbol(
            string name,
            bool isReadOnly,
            TypeSymbol type,
            BoundConstant? constant
        ) : base(name, isReadOnly, type, constant)
        {
        }

        #endregion Constructor
    }
}
