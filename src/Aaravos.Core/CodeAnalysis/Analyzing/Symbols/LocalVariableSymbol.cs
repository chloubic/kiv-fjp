using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Binding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public class LocalVariableSymbol : VariableSymbol
    {

        #region Properties

        public override SymbolKind Kind => SymbolKind.LocalVariable;

        #endregion Properties

        #region Constructor

        internal LocalVariableSymbol(
            string name,
            bool isReadOnly,
            TypeSymbol type,
            BoundConstant? constant
        ) : base(name, isReadOnly, type, constant)
        {
        }

        #endregion Constructor
    }
}
