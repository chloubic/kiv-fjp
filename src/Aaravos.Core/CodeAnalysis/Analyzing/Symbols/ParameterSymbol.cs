using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public sealed class ParameterSymbol : LocalVariableSymbol
    {

        #region Properties

        public override SymbolKind Kind => SymbolKind.Parameter;
        public int Ordinal { get; }

        #endregion Properties

        #region Constructor

        internal ParameterSymbol(
            string name,
            TypeSymbol type,
            int ordinal
        ) : base(name, isReadOnly: true, type, null)
        {
            Ordinal = ordinal;
        }

        #endregion Constructor
    }
}
