using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public abstract class Symbol
    {

        #region Properties

        public abstract SymbolKind Kind { get; }
        public string Name { get; }

        #endregion Properties

        #region Constructor

        private protected Symbol(
            string name
        )
        {
            Name = name;
        }

        #endregion Constructor

        #region Methods

        public void WriteTo(TextWriter writer) => SymbolPrinter.WriteTo(this, writer);

        public override string ToString()
        {
            using (var writer = new StringWriter())
            {
                WriteTo(writer);
                return writer.ToString();
            }
        }

        #endregion Methods

    }
}
