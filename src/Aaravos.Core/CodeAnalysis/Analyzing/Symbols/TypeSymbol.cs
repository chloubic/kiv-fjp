using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Constants;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public sealed class TypeSymbol : Symbol
    {

        #region Properties

        public override SymbolKind Kind => SymbolKind.Type;

        #endregion Properties

        #region Constructor

        private TypeSymbol(
            string name
        ) : base(name)
        {
        }

        #endregion Constructor

        #region Initializers

        public static readonly TypeSymbol Error = new TypeSymbol(KeywordConstants.TYPE_ERROR);
        public static readonly TypeSymbol Any = new TypeSymbol(KeywordConstants.TYPE_ANY);
        public static readonly TypeSymbol Bool = new TypeSymbol(KeywordConstants.TYPE_BOOL);
        public static readonly TypeSymbol Int = new TypeSymbol(KeywordConstants.TYPE_NUMBER);
        public static readonly TypeSymbol String = new TypeSymbol(KeywordConstants.TYPE_STRING);
        public static readonly TypeSymbol Void = new TypeSymbol(KeywordConstants.TYPE_VOID);

        #endregion Initializers

    }
}
