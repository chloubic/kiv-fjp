using Aaravos.Core.CodeAnalysis.Binding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Symbols
{
    public abstract class VariableSymbol : Symbol
    {

        #region Properties

        public bool IsReadOnly { get; }
        public TypeSymbol Type { get; }
        internal BoundConstant? Constant { get; }

        #endregion Properties

        #region Constructor

        internal VariableSymbol(
            string name,
            bool isReadOnly,
            TypeSymbol type,
            BoundConstant? constant
        ) : base(name)
        {
            IsReadOnly = isReadOnly;
            Type = type;
            Constant = isReadOnly ? constant : null;
        }

        #endregion Constructor
    }
}
