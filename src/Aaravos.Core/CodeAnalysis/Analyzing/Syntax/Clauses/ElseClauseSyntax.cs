using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class ElseClauseSyntax : SyntaxNode
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.ElseClause;
        public SyntaxToken ElseKeyword { get; }
        public StatementSyntax ElseStatement { get; }

        #endregion Properties

        #region Constructor

        internal ElseClauseSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken elseKeyword,
            StatementSyntax elseStatement
        ) : base(syntaxTree)
        {
            ElseKeyword = elseKeyword;
            ElseStatement = elseStatement;
        }

        #endregion Constructor
    }
}
