using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class TypeClauseSyntax : SyntaxNode
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.TypeClause;
        public SyntaxToken ColonToken { get; }
        public SyntaxToken Identifier { get; }

        #endregion Properties

        #region Constructor

        internal TypeClauseSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken colonToken,
            SyntaxToken identifier
        ) : base(syntaxTree)
        {
            ColonToken = colonToken;
            Identifier = identifier;
        }

        #endregion Constructor
    }
}
