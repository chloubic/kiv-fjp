using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class CompilationUnitSyntax : SyntaxNode
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.CompilationUnit;
        public ImmutableArray<MemberSyntax> Members { get; }
        public SyntaxToken EndOfFileToken { get; }

        #endregion Properties

        #region Constructor

        internal CompilationUnitSyntax(
            SyntaxTree syntaxTree,
            ImmutableArray<MemberSyntax> members,
            SyntaxToken endOfFileToken
        ) : base(syntaxTree)
        {
            Members = members;
            EndOfFileToken = endOfFileToken;
        }

        #endregion Constructor
    }
}
