using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class FunctionDeclarationSyntax : MemberSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.FunctionDeclaration;

        public SyntaxToken FunctionKeyword { get; }
        public SyntaxToken Identifier { get; }
        public SyntaxToken OpenParenthesisToken { get; }
        public SeparatedSyntaxList<ParameterSyntax> Parameters { get; }
        public SyntaxToken CloseParenthesisToken { get; }
        public TypeClauseSyntax? Type { get; }
        public BlockStatementSyntax Body { get; }

        #endregion Properties

        #region Constructor

        internal FunctionDeclarationSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken functionKeyword,
            SyntaxToken identifier,
            SyntaxToken openParenthesisToken,
            SeparatedSyntaxList<ParameterSyntax> parameters,
            SyntaxToken closeParenthesisToken,
            TypeClauseSyntax? type,
            BlockStatementSyntax body
        ) : base(syntaxTree)
        {
            FunctionKeyword = functionKeyword;
            Identifier = identifier;
            OpenParenthesisToken = openParenthesisToken;
            Parameters = parameters;
            CloseParenthesisToken = closeParenthesisToken;
            Type = type;
            Body = body;
        }

        #endregion Constructor
    }
}
