using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class VariableDeclarationSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.VariableDeclaration;
        public SyntaxToken Keyword { get; }
        public SyntaxToken Identifier { get; }
        public TypeClauseSyntax? TypeClause { get; }
        public SyntaxToken EqualsToken { get; }
        public ExpressionSyntax Initializer { get; }

        #endregion Properties

        #region Constructor

        internal VariableDeclarationSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken keyword,
            SyntaxToken identifier,
            TypeClauseSyntax? typeClause,
            SyntaxToken equalsToken,
            ExpressionSyntax initializer
        ) : base(syntaxTree)
        {
            Keyword = keyword;
            Identifier = identifier;
            TypeClause = typeClause;
            EqualsToken = equalsToken;
            Initializer = initializer;
        }

        #endregion Constructor
    }
}
