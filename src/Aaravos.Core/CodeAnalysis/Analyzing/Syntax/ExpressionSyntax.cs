using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public abstract class ExpressionSyntax : SyntaxNode
    {

        #region Constructor

        private protected ExpressionSyntax(
            SyntaxTree syntaxTree
        ) : base(syntaxTree)
        {
        }

        #endregion Constructor
    }
}
