using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class AssignmentExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.AssignmentExpression;
        public SyntaxToken IdentifierToken { get; }
        public SyntaxToken AssignmentToken { get; }
        public ExpressionSyntax Expression { get; }

        #endregion Properties

        #region Constructor

        public AssignmentExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken identifierToken,
            SyntaxToken assignmentToken,
            ExpressionSyntax expression
        ) : base(syntaxTree)
        {
            IdentifierToken = identifierToken;
            AssignmentToken = assignmentToken;
            Expression = expression;
        }

        #endregion Constructor
    }
}
