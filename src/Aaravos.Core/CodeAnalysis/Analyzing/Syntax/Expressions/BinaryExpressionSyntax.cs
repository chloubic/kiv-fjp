using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class BinaryExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.BinaryExpression;
        public ExpressionSyntax Left { get; }
        public SyntaxToken OperatorToken { get; }
        public ExpressionSyntax Right { get; }

        #endregion Properties

        #region Constructor

        internal BinaryExpressionSyntax(
            SyntaxTree syntaxTree,
            ExpressionSyntax left,
            SyntaxToken operatorToken,
            ExpressionSyntax right
        ) : base(syntaxTree)
        {
            Left = left;
            OperatorToken = operatorToken;
            Right = right;
        }

        #endregion Constructor
    }
}
