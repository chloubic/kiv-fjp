using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class LiteralExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.LiteralExpression;
        public SyntaxToken LiteralToken { get; }
        public object Value { get; }

        #endregion Properties

        #region Constructor

        internal LiteralExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken literalToken
        ) : this(syntaxTree, literalToken, literalToken.Value!)
        {
        }

        internal LiteralExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken literalToken,
            object value
        ) : base(syntaxTree)
        {
            LiteralToken = literalToken;
            Value = value;
        }

        #endregion Constructor
    }
}
