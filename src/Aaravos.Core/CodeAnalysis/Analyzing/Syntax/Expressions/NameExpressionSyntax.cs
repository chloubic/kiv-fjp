using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class NameExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.NameExpression;
        public SyntaxToken IdentifierToken { get; }

        #endregion Properties

        #region Constructor

        internal NameExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken identifierToken
        ) : base(syntaxTree)
        {
            IdentifierToken = identifierToken;
        }

        #endregion Constructor
    }
}
