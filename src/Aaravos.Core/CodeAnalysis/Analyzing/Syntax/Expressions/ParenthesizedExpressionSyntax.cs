using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class ParenthesizedExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.ParenthesizedExpression;
        public SyntaxToken OpenParenthesisToken { get; }
        public ExpressionSyntax Expression { get; }
        public SyntaxToken CloseParenthesisToken { get; }

        #endregion Properties

        #region Constructor

        internal ParenthesizedExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken openParenthesisToken,
            ExpressionSyntax expression,
            SyntaxToken closeParenthesisToken
        ) : base(syntaxTree)
        {
            OpenParenthesisToken = openParenthesisToken;
            Expression = expression;
            CloseParenthesisToken = closeParenthesisToken;
        }

        #endregion Constructor
    }
}
