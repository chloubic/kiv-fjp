using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class UnaryExpressionSyntax : ExpressionSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.UnaryExpression;
        public SyntaxToken OperatorToken { get; }
        public ExpressionSyntax Operand { get; }

        #endregion Properties

        #region Constructor

        internal UnaryExpressionSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken operatorToken,
            ExpressionSyntax operand
        ) : base(syntaxTree)
        {
            OperatorToken = operatorToken;
            Operand = operand;
        }

        #endregion Constructor
    }
}
