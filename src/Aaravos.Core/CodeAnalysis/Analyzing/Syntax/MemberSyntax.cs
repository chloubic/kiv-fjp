using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public abstract class MemberSyntax : SyntaxNode
    {

        #region Constructor

        private protected MemberSyntax(
            SyntaxTree syntaxTree
        ) : base(syntaxTree)
        {
        }

        #endregion Constructor
    }
}
