using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class ParameterSyntax : SyntaxNode
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.Parameter;
        public SyntaxToken Identifier { get; }
        public TypeClauseSyntax Type { get; }

        #endregion Properties

        #region Constructor

        internal ParameterSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken identifier,
            TypeClauseSyntax type
        ) : base(syntaxTree)
        {
            Identifier = identifier;
            Type = type;
        }

        #endregion Constructor
    }
}
