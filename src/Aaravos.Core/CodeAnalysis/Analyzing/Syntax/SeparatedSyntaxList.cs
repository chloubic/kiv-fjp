using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public abstract class SeparatedSyntaxList
    {

        #region Constructor

        private protected SeparatedSyntaxList()
        {
        }

        #endregion Constructor

        #region Methods

        public abstract ImmutableArray<SyntaxNode> GetWithSeparators();

        #endregion Methods

    }

    public sealed class SeparatedSyntaxList<T> : SeparatedSyntaxList, IEnumerable<T>
        where T : SyntaxNode
    {

        #region Properties

        private readonly ImmutableArray<SyntaxNode> NodesAndSeparators;

        public int Count => (NodesAndSeparators.Length + 1) / 2;

        public T this[int index] => (T)NodesAndSeparators[index * 2];

        #endregion Properties

        #region Constructor


        internal SeparatedSyntaxList(
            ImmutableArray<SyntaxNode> nodesAndSeparators
        )
        {
            NodesAndSeparators = nodesAndSeparators;
        }

        #endregion Constructor

        #region Methods

        public SyntaxToken GetSeparator(int index)
        {
            if (index < 0 || index >= Count - 1)
                throw new ArgumentOutOfRangeException(nameof(index));

            return (SyntaxToken)NodesAndSeparators[index * 2 + 1];
        }

        public override ImmutableArray<SyntaxNode> GetWithSeparators() => NodesAndSeparators;

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; i++)
                yield return this[i];
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #endregion Methods

    }
}
