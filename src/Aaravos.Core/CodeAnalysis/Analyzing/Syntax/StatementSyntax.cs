using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public abstract class StatementSyntax : SyntaxNode
    {

        #region Constructor

        private protected StatementSyntax(
            SyntaxTree syntaxTree
        ) : base(syntaxTree)
        {
        }

        #endregion Constructor
    }
}
