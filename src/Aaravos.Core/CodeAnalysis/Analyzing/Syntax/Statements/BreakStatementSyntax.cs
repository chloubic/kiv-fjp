using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    internal sealed partial class BreakStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.BreakStatement;
        public SyntaxToken Keyword { get; }

        #endregion Properties

        #region Constructor

        internal BreakStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken keyword
        ) : base(syntaxTree)
        {
            Keyword = keyword;
        }

        #endregion Constructor
    }
}
