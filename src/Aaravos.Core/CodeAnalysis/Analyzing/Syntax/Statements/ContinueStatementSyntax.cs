using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    internal sealed partial class ContinueStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.ContinueStatement;
        public SyntaxToken Keyword { get; }

        #endregion Properties

        #region Constructor

        internal ContinueStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken keyword
        ) : base(syntaxTree)
        {
            Keyword = keyword;
        }

        #endregion Constructor
    }
}
