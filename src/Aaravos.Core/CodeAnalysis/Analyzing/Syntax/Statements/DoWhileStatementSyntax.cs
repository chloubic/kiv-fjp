using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class DoWhileStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.DoWhileStatement;
        public SyntaxToken DoKeyword { get; }
        public StatementSyntax Body { get; }
        public SyntaxToken WhileKeyword { get; }
        public SyntaxToken OpenParenthesisToken { get; }
        public ExpressionSyntax Condition { get; }
        public SyntaxToken CloseParenthesisToken { get; }

        #endregion Properties

        #region Constructor

        internal DoWhileStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken doKeyword,
            StatementSyntax body,
            SyntaxToken whileKeyword,
            SyntaxToken openParenthesisToken,
            ExpressionSyntax condition,
            SyntaxToken closeParenthesisToken
        ) : base(syntaxTree)
        {
            DoKeyword = doKeyword;
            Body = body;
            WhileKeyword = whileKeyword;
            OpenParenthesisToken = openParenthesisToken;
            Condition = condition;
            CloseParenthesisToken = closeParenthesisToken;
        }

        #endregion Constructor
    }
}
