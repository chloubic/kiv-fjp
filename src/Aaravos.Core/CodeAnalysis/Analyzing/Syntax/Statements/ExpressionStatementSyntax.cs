using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class ExpressionStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.ExpressionStatement;
        public ExpressionSyntax Expression { get; }

        #endregion Properties

        #region Constructor

        internal ExpressionStatementSyntax(
            SyntaxTree syntaxTree,
            ExpressionSyntax expression
        ) : base(syntaxTree)
        {
            Expression = expression;
        }

        #endregion Constructor
    }
}
