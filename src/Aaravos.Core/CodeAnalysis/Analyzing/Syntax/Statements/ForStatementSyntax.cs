using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class ForStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.ForStatement;
        public SyntaxToken Keyword { get; }
        public SyntaxToken OpenParenthesisToken { get; }
        public SyntaxToken Identifier { get; }
        public SyntaxToken EqualsToken { get; }
        public ExpressionSyntax LowerBound { get; }
        public SyntaxToken ToKeyword { get; }
        public ExpressionSyntax UpperBound { get; }
        public SyntaxToken CloseParenthesisToken { get; }
        public StatementSyntax Body { get; }

        #endregion Properties

        #region Constructor

        internal ForStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken keyword,
            SyntaxToken openParenthesisToken,
            SyntaxToken identifier,
            SyntaxToken equalsToken,
            ExpressionSyntax lowerBound,
            SyntaxToken toKeyword,
            ExpressionSyntax upperBound,
            SyntaxToken closeParenthesisToken,
            StatementSyntax body
        ) : base(syntaxTree)
        {
            Keyword = keyword;
            OpenParenthesisToken = openParenthesisToken;
            Identifier = identifier;
            EqualsToken = equalsToken;
            LowerBound = lowerBound;
            ToKeyword = toKeyword;
            UpperBound = upperBound;
            CloseParenthesisToken = closeParenthesisToken;
            Body = body;
        }

        #endregion Constructor
    }
}
