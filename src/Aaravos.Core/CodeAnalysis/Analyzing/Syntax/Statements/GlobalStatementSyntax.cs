using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class GlobalStatementSyntax : MemberSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.GlobalStatement;
        public StatementSyntax Statement { get; }

        #endregion Properties

        #region Constructor

        internal GlobalStatementSyntax(
            SyntaxTree syntaxTree,
            StatementSyntax statement
        ) : base(syntaxTree)
        {
            Statement = statement;
        }

        #endregion Constructor
    }
}
