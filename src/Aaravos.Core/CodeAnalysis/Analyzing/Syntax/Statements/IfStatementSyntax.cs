using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class IfStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.IfStatement;
        public SyntaxToken IfKeyword { get; }
        public SyntaxToken OpenParenthesisToken { get; }
        public ExpressionSyntax Condition { get; }
        public SyntaxToken CloseParenthesisToken { get; }
        public StatementSyntax ThenStatement { get; }
        public ElseClauseSyntax? ElseClause { get; }

        #endregion Properties

        #region Constructor

        internal IfStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken ifKeyword,
            SyntaxToken openParenthesisToken,
            ExpressionSyntax condition,
            SyntaxToken closeParenthesisToken,
            StatementSyntax thenStatement,
            ElseClauseSyntax? elseClause
        ) : base(syntaxTree)
        {
            IfKeyword = ifKeyword;
            OpenParenthesisToken = openParenthesisToken;
            Condition = condition;
            CloseParenthesisToken = closeParenthesisToken;
            ThenStatement = thenStatement;
            ElseClause = elseClause;
        }

        #endregion Constructor
    }
}
