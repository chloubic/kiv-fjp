using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed partial class WhileStatementSyntax : StatementSyntax
    {

        #region Properties

        public override SyntaxKind Kind => SyntaxKind.WhileStatement;
        public SyntaxToken WhileKeyword { get; }
        public SyntaxToken OpenParenthesisToken { get; }
        public ExpressionSyntax Condition { get; }
        public SyntaxToken CloseParenthesisToken { get; }
        public StatementSyntax Body { get; }

        #endregion Properties

        #region Constructor

        internal WhileStatementSyntax(
            SyntaxTree syntaxTree,
            SyntaxToken whileKeyword,
            SyntaxToken openParenthesisToken,
            ExpressionSyntax condition,
            SyntaxToken closeParenthesisToken,
            StatementSyntax body
        ) : base(syntaxTree)
        {
            WhileKeyword = whileKeyword;
            OpenParenthesisToken = openParenthesisToken;
            Condition = condition;
            CloseParenthesisToken = closeParenthesisToken;
            Body = body;
        }

        #endregion Constructor
    }
}
