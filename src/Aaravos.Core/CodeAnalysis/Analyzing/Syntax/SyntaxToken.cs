using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Texting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Analyzing.Syntax
{
    public sealed class SyntaxToken : SyntaxNode
    {

        #region Properties


        /// <summary>
        /// A token is missing if it was inserted by the parser and doesn't appear in source.
        /// </summary>
        public bool IsMissing { get; }
        public override SyntaxKind Kind { get; }
        public int Position { get; }
        public string Text { get; }
        public object? Value { get; }
        public override TextSpan Span => new TextSpan(Position, Text.Length);
        public override TextSpan FullSpan
        {
            get
            {
                var start = LeadingTrivia.Length == 0
                                ? Span.Start
                                : LeadingTrivia.First().Span.Start;
                var end = TrailingTrivia.Length == 0
                                ? Span.End
                                : TrailingTrivia.Last().Span.End;
                return TextSpan.FromBounds(start, end);
            }
        }

        public ImmutableArray<SyntaxTrivia> LeadingTrivia { get; }
        public ImmutableArray<SyntaxTrivia> TrailingTrivia { get; }

        #endregion Properties

        #region Constructor

        internal SyntaxToken(
            SyntaxTree syntaxTree,
            SyntaxKind kind,
            int position,
            string? text,
            object? value,
            ImmutableArray<SyntaxTrivia> leadingTrivia,
            ImmutableArray<SyntaxTrivia> trailingTrivia
        ) : base(syntaxTree)
        {
            Kind = kind;
            Position = position;
            Text = text ?? string.Empty;
            IsMissing = text == null;
            Value = value;
            LeadingTrivia = leadingTrivia;
            TrailingTrivia = trailingTrivia;
        }

        #endregion Constructor

        #region Methods

        public override IEnumerable<SyntaxNode> GetChildren()
             => Array.Empty<SyntaxNode>();

        #endregion Methods

    }
}
