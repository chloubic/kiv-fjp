using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal sealed class BoundConstant
    {

        #region Properties

        public object Value { get; }

        #endregion Properties

        #region Constructor

        public BoundConstant(
            object value
        )
        {
            Value = value;
        }

        #endregion Constructor
    }
}
