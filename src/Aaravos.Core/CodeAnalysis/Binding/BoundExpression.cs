using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal abstract class BoundExpression : BoundNode
    {

        #region Properties

        public abstract TypeSymbol Type { get; }
        public virtual BoundConstant? ConstantValue => null;

        #endregion Properties

        #region Constructor

        protected BoundExpression(
            SyntaxNode syntax
        )
            : base(syntax)
        {
        }

        #endregion Constructor
    }
}
