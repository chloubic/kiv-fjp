using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Diagnosting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal sealed class BoundGlobalScope
    {

        #region Properties

        public BoundGlobalScope? Previous { get; }
        public ImmutableArray<Diagnostic> Diagnostics { get; }
        public FunctionSymbol? MainFunction { get; }
        public FunctionSymbol? ScriptFunction { get; }
        public ImmutableArray<FunctionSymbol> Functions { get; }
        public ImmutableArray<VariableSymbol> Variables { get; }
        public ImmutableArray<BoundStatement> Statements { get; }

        #endregion Properties

        #region Constructor

        public BoundGlobalScope(
            BoundGlobalScope? previous,
            ImmutableArray<Diagnostic> diagnostics,
            FunctionSymbol? mainFunction,
            FunctionSymbol? scriptFunction,
            ImmutableArray<FunctionSymbol> functions,
            ImmutableArray<VariableSymbol> variables,
            ImmutableArray<BoundStatement> statements
        )
        {
            Previous = previous;
            Diagnostics = diagnostics;
            MainFunction = mainFunction;
            ScriptFunction = scriptFunction;
            Functions = functions;
            Variables = variables;
            Statements = statements;
        }

        #endregion Constructor
    }
}
