using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal sealed class BoundLabel
    {

        #region Properties

        public string Name { get; }

        #endregion Properties

        #region Constructor

        internal BoundLabel(
            string name
        )
        {
            Name = name;
        }

        #endregion Constructor

        #region Methods

        public override string ToString() => Name;

        #endregion Methods

    }
}
