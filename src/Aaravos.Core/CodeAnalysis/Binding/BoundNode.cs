using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using Aaravos.Core.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal abstract class BoundNode
    {

        #region Properties

        public abstract BoundNodeKind Kind { get; }
        public SyntaxNode Syntax { get; }

        #endregion Properties

        #region Constructor

        protected BoundNode(
            SyntaxNode syntax
        )
        {
            Syntax = syntax;
        }

        #endregion Constructor

        #region Methods

        public override string ToString()
        {
            using (var writer = new StringWriter())
            {
                this.WriteTo(writer);
                return writer.ToString();
            }
        }

        #endregion Methods

    }
}
