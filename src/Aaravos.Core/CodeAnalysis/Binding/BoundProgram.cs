using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Binding.Statements;
using Aaravos.Core.CodeAnalysis.Diagnosting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal sealed class BoundProgram
    {

        #region Properties

        public BoundProgram? Previous { get; }
        public ImmutableArray<Diagnostic> Diagnostics { get; }
        public FunctionSymbol? MainFunction { get; }
        public FunctionSymbol? ScriptFunction { get; }
        public ImmutableDictionary<FunctionSymbol, BoundBlockStatement> Functions { get; }

        #endregion Properties

        #region Constructor

        public BoundProgram(
            BoundProgram? previous,
            ImmutableArray<Diagnostic> diagnostics,
            FunctionSymbol? mainFunction,
            FunctionSymbol? scriptFunction,
            ImmutableDictionary<FunctionSymbol, BoundBlockStatement> functions
        )
        {
            Previous = previous;
            Diagnostics = diagnostics;
            MainFunction = mainFunction;
            ScriptFunction = scriptFunction;
            Functions = functions;
        }

        #endregion Constructor

    }
}
