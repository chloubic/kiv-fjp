using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal sealed class BoundScope
    {

        #region Properties

        private Dictionary<string, Symbol>? Symbols;

        public BoundScope? Parent { get; }

        #endregion Properties

        #region Constructor

        public BoundScope(
            BoundScope? parent
        )
        {
            Parent = parent;
        }

        #endregion Constructor

        #region Methods

        public bool TryDeclareVariable(VariableSymbol variable)
            => TryDeclareSymbol(variable);

        public bool TryDeclareFunction(FunctionSymbol function)
            => TryDeclareSymbol(function);

        private bool TryDeclareSymbol<TSymbol>(TSymbol symbol)
            where TSymbol : Symbol
        {
            if (Symbols == null)
                Symbols = new Dictionary<string, Symbol>();
            else if (Symbols.ContainsKey(symbol.Name))
                return false;

            Symbols.Add(symbol.Name, symbol);
            return true;
        }

        public Symbol? TryLookupSymbol(string name)
        {
            if (Symbols != null && Symbols.TryGetValue(name, out var symbol))
                return symbol;

            return Parent?.TryLookupSymbol(name);
        }

        public ImmutableArray<VariableSymbol> GetDeclaredVariables()
            => GetDeclaredSymbols<VariableSymbol>();

        public ImmutableArray<FunctionSymbol> GetDeclaredFunctions()
            => GetDeclaredSymbols<FunctionSymbol>();

        private ImmutableArray<TSymbol> GetDeclaredSymbols<TSymbol>()
            where TSymbol : Symbol
        {
            if (Symbols == null)
                return ImmutableArray<TSymbol>.Empty;

            return Symbols.Values.OfType<TSymbol>().ToImmutableArray();
        }

        #endregion Methods

    }
}
