using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding
{
    internal abstract class BoundStatement : BoundNode
    {

        #region Constructor

        protected BoundStatement(
            SyntaxNode syntax
        ) : base(syntax)
        {
        }

        #endregion Constructor

    }
}
