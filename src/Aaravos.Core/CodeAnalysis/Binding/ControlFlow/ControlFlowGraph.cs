using Aaravos.Core.CodeAnalysis.Binding.Expressions;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using Aaravos.Core.CodeAnalysis.Binding.Statements;
using Aaravos.Core.IO;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.ControlFlow
{
    internal sealed class ControlFlowGraph
    {

        #region Properties

        public BasicBlock Start { get; }
        public BasicBlock End { get; }
        public List<BasicBlock> Blocks { get; }
        public List<BasicBlockBranch> Branches { get; }

        #endregion Properties

        #region Constructor

        private ControlFlowGraph(
            BasicBlock start,
            BasicBlock end,
            List<BasicBlock> blocks,
            List<BasicBlockBranch> branches
        )
        {
            Start = start;
            End = end;
            Blocks = blocks;
            Branches = branches;
        }

        #endregion Constructor

        #region Inner classes

        public sealed class BasicBlock
        {


            #region Properties

            public bool IsStart { get; }
            public bool IsEnd { get; }
            public List<BoundStatement> Statements { get; } = new List<BoundStatement>();
            public List<BasicBlockBranch> Incoming { get; } = new List<BasicBlockBranch>();
            public List<BasicBlockBranch> Outgoing { get; } = new List<BasicBlockBranch>();

            #endregion Properties

            #region Constructor

            public BasicBlock()
            {
            }

            public BasicBlock(
                bool isStart
            )
            {
                IsStart = isStart;
                IsEnd = !isStart;
            }

            #endregion Constructor

            #region Methods

            public override string ToString()
            {
                if (IsStart)
                    return "<Start>";

                if (IsEnd)
                    return "<End>";

                using (var writer = new StringWriter())
                using (var indentedWriter = new IndentedTextWriter(writer))
                {
                    foreach (var statement in Statements)
                        statement.WriteTo(indentedWriter);

                    return writer.ToString();
                }
            }

            #endregion Methods

        }

        public sealed class BasicBlockBranch
        {

            #region Properties

            public BasicBlock From { get; }
            public BasicBlock To { get; }
            public BoundExpression? Condition { get; }

            #endregion Properties

            #region Constructor

            public BasicBlockBranch(
                BasicBlock from,
                BasicBlock to,
                BoundExpression? condition
            )
            {
                From = from;
                To = to;
                Condition = condition;
            }

            #endregion Constructor

            #region Methods

            public override string ToString()
            {
                if (Condition == null)
                    return string.Empty;

                return Condition.ToString();
            }

            #endregion Methods

        }

        public sealed class BasicBlockBuilder
        {

            #region Properties

            private List<BoundStatement> Statements = new List<BoundStatement>();
            private List<BasicBlock> Blocks = new List<BasicBlock>();

            #endregion Properties

            #region Methods

            public List<BasicBlock> Build(BoundBlockStatement block)
            {
                foreach (var statement in block.Statements)
                {
                    switch (statement.Kind)
                    {
                        case BoundNodeKind.LabelStatement:
                            StartBlock();
                            Statements.Add(statement);
                            break;
                        case BoundNodeKind.GotoStatement:
                        case BoundNodeKind.ConditionalGotoStatement:
                        case BoundNodeKind.ReturnStatement:
                            Statements.Add(statement);
                            StartBlock();
                            break;
                        case BoundNodeKind.NopStatement:
                        case BoundNodeKind.VariableDeclaration:
                        case BoundNodeKind.ExpressionStatement:
                            Statements.Add(statement);
                            break;
                        default:
                            throw new Exception($"Unexpected statement: {statement.Kind}");
                    }
                }

                EndBlock();

                return Blocks.ToList();
            }

            private void StartBlock()
                => EndBlock();

            private void EndBlock()
            {
                if (Statements.Count > 0)
                {
                    var block = new BasicBlock();
                    block.Statements.AddRange(Statements);
                    Blocks.Add(block);
                    Statements.Clear();
                }
            }

            #endregion Methods
        }

        public sealed class GraphBuilder
        {

            #region Properties

            private Dictionary<BoundStatement, BasicBlock> BlockFromStatement = new Dictionary<BoundStatement, BasicBlock>();
            private Dictionary<BoundLabel, BasicBlock> BlockFromLabel = new Dictionary<BoundLabel, BasicBlock>();
            private List<BasicBlockBranch> Branches = new List<BasicBlockBranch>();
            private BasicBlock Start = new BasicBlock(isStart: true);
            private BasicBlock End = new BasicBlock(isStart: false);

            #endregion Properties

            #region Methods

            public ControlFlowGraph Build(List<BasicBlock> blocks)
            {
                if (!blocks.Any())
                    Connect(Start, End);
                else
                    Connect(Start, blocks.First());

                foreach (var block in blocks)
                {
                    foreach (var statement in block.Statements)
                    {
                        BlockFromStatement.Add(statement, block);
                        if (statement is BoundLabelStatement labelStatement)
                            BlockFromLabel.Add(labelStatement.Label, block);
                    }
                }

                for (int i = 0; i < blocks.Count; i++)
                {
                    var current = blocks[i];
                    var next = i == blocks.Count - 1 ? End : blocks[i + 1];

                    foreach (var statement in current.Statements)
                    {
                        var isLastStatementInBlock = statement == current.Statements.Last();
                        switch (statement.Kind)
                        {
                            case BoundNodeKind.GotoStatement:
                                var gs = (BoundGotoStatement)statement;
                                var toBlock = BlockFromLabel[gs.Label];
                                Connect(current, toBlock);
                                break;
                            case BoundNodeKind.ConditionalGotoStatement:
                                var cgs = (BoundConditionalGotoStatement)statement;
                                var thenBlock = BlockFromLabel[cgs.Label];
                                var elseBlock = next;
                                var negatedCondition = Negate(cgs.Condition);
                                var thenCondition = cgs.JumpIfTrue ? cgs.Condition : negatedCondition;
                                var elseCondition = cgs.JumpIfTrue ? negatedCondition : cgs.Condition;
                                Connect(current, thenBlock, thenCondition);
                                Connect(current, elseBlock, elseCondition);
                                break;
                            case BoundNodeKind.ReturnStatement:
                                Connect(current, End);
                                break;
                            case BoundNodeKind.NopStatement:
                            case BoundNodeKind.VariableDeclaration:
                            case BoundNodeKind.LabelStatement:
                            case BoundNodeKind.ExpressionStatement:
                                if (isLastStatementInBlock)
                                    Connect(current, next);
                                break;
                            default:
                                throw new Exception($"Unexpected statement: {statement.Kind}");
                        }
                    }
                }

            ScanAgain:
                foreach (var block in blocks)
                {
                    if (!block.Incoming.Any())
                    {
                        RemoveBlock(blocks, block);
                        goto ScanAgain;
                    }
                }

                blocks.Insert(0, Start);
                blocks.Add(End);

                return new ControlFlowGraph(Start, End, blocks, Branches);
            }

            private void Connect(BasicBlock from, BasicBlock to, BoundExpression? condition = null)
            {
                if (condition is BoundLiteralExpression l)
                {
                    var value = (bool)l.Value;
                    if (value)
                        condition = null;
                    else
                        return;
                }

                var branch = new BasicBlockBranch(from, to, condition);
                from.Outgoing.Add(branch);
                to.Incoming.Add(branch);
                Branches.Add(branch);
            }

            private void RemoveBlock(List<BasicBlock> blocks, BasicBlock block)
            {
                foreach (var branch in block.Incoming)
                {
                    branch.From.Outgoing.Remove(branch);
                    Branches.Remove(branch);
                }

                foreach (var branch in block.Outgoing)
                {
                    branch.To.Incoming.Remove(branch);
                    Branches.Remove(branch);
                }

                blocks.Remove(block);
            }

            private BoundExpression Negate(BoundExpression condition)
            {
                var negated = BoundNodeFactory.Not(condition.Syntax, condition);
                if (negated.ConstantValue != null)
                    return new BoundLiteralExpression(condition.Syntax, negated.ConstantValue.Value);

                return negated;
            }

            #endregion Methods
        }

        #endregion Inner classes

        #region Methods

        public void WriteTo(TextWriter writer)
        {
            string Quote(string text)
            {
                return "\"" + text.TrimEnd().Replace("\\", "\\\\").Replace("\"", "\\\"").Replace(Environment.NewLine, "\\l") + "\"";
            }

            writer.WriteLine("digraph G {");

            var blockIds = new Dictionary<BasicBlock, string>();

            for (int i = 0; i < Blocks.Count; i++)
            {
                var id = $"N{i}";
                blockIds.Add(Blocks[i], id);
            }

            foreach (var block in Blocks)
            {
                var id = blockIds[block];
                var label = Quote(block.ToString());
                writer.WriteLine($"    {id} [label = {label}, shape = box]");
            }

            foreach (var branch in Branches)
            {
                var fromId = blockIds[branch.From];
                var toId = blockIds[branch.To];
                var label = Quote(branch.ToString());
                writer.WriteLine($"    {fromId} -> {toId} [label = {label}]");
            }

            writer.WriteLine("}");
        }

        public static ControlFlowGraph Create(BoundBlockStatement body)
        {
            var basicBlockBuilder = new BasicBlockBuilder();
            var blocks = basicBlockBuilder.Build(body);

            var graphBuilder = new GraphBuilder();
            return graphBuilder.Build(blocks);
        }

        public static bool AllPathsReturn(BoundBlockStatement body)
        {
            var graph = Create(body);

            foreach (var branch in graph.End.Incoming)
            {
                var lastStatement = branch.From.Statements.LastOrDefault();
                if (lastStatement == null || lastStatement.Kind != BoundNodeKind.ReturnStatement)
                    return false;
            }

            return true;
        }

        #endregion Methods

    }
}
