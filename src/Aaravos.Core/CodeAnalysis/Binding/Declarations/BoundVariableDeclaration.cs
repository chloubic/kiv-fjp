using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Declarations
{
    internal sealed class BoundVariableDeclaration : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.VariableDeclaration;
        public VariableSymbol Variable { get; }
        public BoundExpression Initializer { get; }

        #endregion Properties

        #region Constructor

        public BoundVariableDeclaration(
            SyntaxNode syntax,
            VariableSymbol variable,
            BoundExpression initializer
        ) : base(syntax)
        {
            Variable = variable;
            Initializer = initializer;
        }

        #endregion Constructor
    }
}
