using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundAssignmentExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.AssignmentExpression;
        public override TypeSymbol Type => Expression.Type;
        public VariableSymbol Variable { get; }
        public BoundExpression Expression { get; }

        #endregion Properties

        #region Constructor

        public BoundAssignmentExpression(
            SyntaxNode syntax,
            VariableSymbol variable,
            BoundExpression expression
        ) : base(syntax)
        {
            Variable = variable;
            Expression = expression;
        }

        #endregion Constructor
    }
}
