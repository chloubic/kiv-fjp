using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using Aaravos.Core.CodeAnalysis.Binding.Operators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundBinaryExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.BinaryExpression;
        public override TypeSymbol Type => Op.Type;
        public BoundExpression Left { get; }
        public BoundBinaryOperator Op { get; }
        public BoundExpression Right { get; }
        public override BoundConstant? ConstantValue { get; }

        #endregion Properties

        #region Constructor

        public BoundBinaryExpression(
            SyntaxNode syntax,
            BoundExpression left,
            BoundBinaryOperator op,
            BoundExpression right
        ) : base(syntax)
        {
            Left = left;
            Op = op;
            Right = right;
            ConstantValue = ConstantFolding.Fold(left, op, right);
        }

        #endregion Constructor
    }
}
