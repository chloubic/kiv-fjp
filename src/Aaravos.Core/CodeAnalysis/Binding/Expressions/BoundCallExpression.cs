using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundCallExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.CallExpression;
        public override TypeSymbol Type => Function.Type;
        public FunctionSymbol Function { get; }
        public ImmutableArray<BoundExpression> Arguments { get; }

        #endregion Properties

        #region Constructor

        public BoundCallExpression(
            SyntaxNode syntax,
            FunctionSymbol function,
            ImmutableArray<BoundExpression> arguments
        ) : base(syntax)
        {
            Function = function;
            Arguments = arguments;
        }

        #endregion Constructor
    }
}
