using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using Aaravos.Core.CodeAnalysis.Binding.Operators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundCompoundAssignmentExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.CompoundAssignmentExpression;
        public override TypeSymbol Type => Expression.Type;
        public VariableSymbol Variable { get; }
        public BoundBinaryOperator Op { get; }
        public BoundExpression Expression { get; }

        #endregion Properties

        #region Constructor

        public BoundCompoundAssignmentExpression(
            SyntaxNode syntax,
            VariableSymbol variable,
            BoundBinaryOperator op,
            BoundExpression expression
        ) : base(syntax)
        {
            Variable = variable;
            Op = op;
            Expression = expression;
        }

        #endregion Constructor
    }
}
