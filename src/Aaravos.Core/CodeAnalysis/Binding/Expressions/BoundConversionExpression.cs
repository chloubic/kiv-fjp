using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundConversionExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.ConversionExpression;
        public override TypeSymbol Type { get; }
        public BoundExpression Expression { get; }

        #endregion Properties

        #region Constructor

        public BoundConversionExpression(
            SyntaxNode syntax,
            TypeSymbol type,
            BoundExpression expression
        ) : base(syntax)
        {
            Type = type;
            Expression = expression;
        }

        #endregion Constructor
    }
}
