using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using Aaravos.Core.CodeAnalysis.Binding.Operators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Expressions
{
    internal sealed class BoundUnaryExpression : BoundExpression
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.UnaryExpression;
        public override TypeSymbol Type => Op.Type;
        public BoundUnaryOperator Op { get; }
        public BoundExpression Operand { get; }
        public override BoundConstant? ConstantValue { get; }

        #endregion Properties

        #region Constructor

        public BoundUnaryExpression(
            SyntaxNode syntax,
            BoundUnaryOperator op,
            BoundExpression operand
        ) : base(syntax)
        {
            Op = op;
            Operand = operand;
            ConstantValue = ConstantFolding.Fold(op, operand);
        }

        #endregion Constructor
    }
}
