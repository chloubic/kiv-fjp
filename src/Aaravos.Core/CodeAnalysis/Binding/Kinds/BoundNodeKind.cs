using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Kinds
{
    internal enum BoundNodeKind
    {

        #region Statements

        BlockStatement,
        NopStatement,
        VariableDeclaration,
        IfStatement,
        WhileStatement,
        DoWhileStatement,
        ForStatement,
        LabelStatement,
        GotoStatement,
        ConditionalGotoStatement,
        ReturnStatement,
        ExpressionStatement,

        #endregion Statements

        #region Expressions

        ErrorExpression,
        LiteralExpression,
        VariableExpression,
        AssignmentExpression,
        CompoundAssignmentExpression,
        UnaryExpression,
        BinaryExpression,
        CallExpression,
        ConversionExpression,

        #endregion Expressions

    }
}
