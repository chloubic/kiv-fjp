﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Kinds
{
    internal enum BoundUnaryOperatorKind
    {
        Identity,
        Negation,
        LogicalNegation,
        OnesComplement
    }
}
