using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Operators
{
    internal sealed class BoundUnaryOperator
    {

        #region Properties

        public SyntaxKind SyntaxKind { get; }
        public BoundUnaryOperatorKind Kind { get; }
        public TypeSymbol OperandType { get; }
        public TypeSymbol Type { get; }

        #endregion Properties


        #region Constructor

        private BoundUnaryOperator(
            SyntaxKind syntaxKind,
            BoundUnaryOperatorKind kind,
            TypeSymbol operandType
        ) : this(syntaxKind, kind, operandType, operandType)
        {
        }

        private BoundUnaryOperator(
            SyntaxKind syntaxKind,
            BoundUnaryOperatorKind kind,
            TypeSymbol operandType,
            TypeSymbol resultType
        )
        {
            SyntaxKind = syntaxKind;
            Kind = kind;
            OperandType = operandType;
            Type = resultType;
        }

        #endregion Constructor

        #region Initializers

        private static BoundUnaryOperator[] Operators =
        {
            new BoundUnaryOperator(SyntaxKind.BangToken, BoundUnaryOperatorKind.LogicalNegation, TypeSymbol.Bool),

            new BoundUnaryOperator(SyntaxKind.PlusToken, BoundUnaryOperatorKind.Identity, TypeSymbol.Int),
            new BoundUnaryOperator(SyntaxKind.MinusToken, BoundUnaryOperatorKind.Negation, TypeSymbol.Int),
            new BoundUnaryOperator(SyntaxKind.TildeToken, BoundUnaryOperatorKind.OnesComplement, TypeSymbol.Int),
        };

        #endregion Initializers

        #region Methods

        public static BoundUnaryOperator? Bind(SyntaxKind syntaxKind, TypeSymbol operandType)
        {
            foreach (var op in Operators)
            {
                if (op.SyntaxKind == syntaxKind && op.OperandType == operandType)
                    return op;
            }

            return null;
        }

        #endregion Methods

    }
}
