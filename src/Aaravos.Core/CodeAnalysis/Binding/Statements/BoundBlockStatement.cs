using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundBlockStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.BlockStatement;
        public ImmutableArray<BoundStatement> Statements { get; }

        #endregion Properties

        #region Constructor

        public BoundBlockStatement(
            SyntaxNode syntax,
            ImmutableArray<BoundStatement> statements
        ) : base(syntax)
        {
            Statements = statements;
        }

        #endregion Constructor

    }
}
