using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundConditionalGotoStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.ConditionalGotoStatement;
        public BoundLabel Label { get; }
        public BoundExpression Condition { get; }
        public bool JumpIfTrue { get; }

        #endregion Properties

        #region Constructor

        public BoundConditionalGotoStatement(
            SyntaxNode syntax,
            BoundLabel label,
            BoundExpression condition,
            bool jumpIfTrue = true
        ) : base(syntax)
        {
            Label = label;
            Condition = condition;
            JumpIfTrue = jumpIfTrue;
        }

        #endregion Constructor
    }
}
