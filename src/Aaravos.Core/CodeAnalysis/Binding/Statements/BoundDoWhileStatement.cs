using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundDoWhileStatement : BoundLoopStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.DoWhileStatement;
        public BoundStatement Body { get; }
        public BoundExpression Condition { get; }

        #endregion Properties

        #region Constructor

        public BoundDoWhileStatement(
            SyntaxNode syntax,
            BoundStatement body,
            BoundExpression condition,
            BoundLabel breakLabel,
            BoundLabel continueLabel
        ) : base(syntax, breakLabel, continueLabel)
        {
            Body = body;
            Condition = condition;
        }

        #endregion Constructor
    }
}
