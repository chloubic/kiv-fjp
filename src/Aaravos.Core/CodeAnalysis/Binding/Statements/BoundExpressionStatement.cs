using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundExpressionStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.ExpressionStatement;
        public BoundExpression Expression { get; }

        #endregion Properties

        #region Constructor

        public BoundExpressionStatement(
            SyntaxNode syntax,
            BoundExpression expression
        ) : base(syntax)
        {
            Expression = expression;
        }

        #endregion Constructor
    }
}
