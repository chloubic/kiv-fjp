using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundForStatement : BoundLoopStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.ForStatement;
        public VariableSymbol Variable { get; }
        public BoundExpression LowerBound { get; }
        public BoundExpression UpperBound { get; }
        public BoundStatement Body { get; }

        #endregion Properties

        #region Constructor

        public BoundForStatement(
            SyntaxNode syntax,
            VariableSymbol variable,
            BoundExpression lowerBound,
            BoundExpression upperBound,
            BoundStatement body,
            BoundLabel breakLabel,
            BoundLabel continueLabel
        ) : base(syntax, breakLabel, continueLabel)
        {
            Variable = variable;
            LowerBound = lowerBound;
            UpperBound = upperBound;
            Body = body;
        }

        #endregion Constructor

    }
}
