using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundGotoStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.GotoStatement;
        public BoundLabel Label { get; }

        #endregion Properties

        #region Constructor

        public BoundGotoStatement(
            SyntaxNode syntax,
            BoundLabel label
        ) : base(syntax)
        {
            Label = label;
        }

        #endregion Constructor
    }
}
