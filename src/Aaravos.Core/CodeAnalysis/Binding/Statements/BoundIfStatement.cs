using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundIfStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.IfStatement;
        public BoundExpression Condition { get; }
        public BoundStatement ThenStatement { get; }
        public BoundStatement? ElseStatement { get; }

        #endregion Properties

        #region Constructor

        public BoundIfStatement(
            SyntaxNode syntax,
            BoundExpression condition,
            BoundStatement thenStatement,
            BoundStatement? elseStatement
        ) : base(syntax)
        {
            Condition = condition;
            ThenStatement = thenStatement;
            ElseStatement = elseStatement;
        }

        #endregion Constructor
    }
}
