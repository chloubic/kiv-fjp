using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal abstract class BoundLoopStatement : BoundStatement
    {

        #region Properties

        public BoundLabel BreakLabel { get; }
        public BoundLabel ContinueLabel { get; }

        #endregion Properties

        #region Constructor

        protected BoundLoopStatement(
            SyntaxNode syntax,
            BoundLabel breakLabel,
            BoundLabel continueLabel
        ) : base(syntax)
        {
            BreakLabel = breakLabel;
            ContinueLabel = continueLabel;
        }

        #endregion Constructor
    }
}
