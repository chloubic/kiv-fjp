using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundNopStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.NopStatement;

        #endregion Properties

        #region Constructor

        public BoundNopStatement(
            SyntaxNode syntax
        ) : base(syntax)
        {
        }

        #endregion Constructor
    }
}
