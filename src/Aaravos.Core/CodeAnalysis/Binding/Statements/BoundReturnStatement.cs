using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundReturnStatement : BoundStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.ReturnStatement;
        public BoundExpression? Expression { get; }

        #endregion Properties

        #region Constructor

        public BoundReturnStatement(
            SyntaxNode syntax,
            BoundExpression? expression
        ) : base(syntax)
        {
            Expression = expression;
        }

        #endregion Constructor
    }
}
