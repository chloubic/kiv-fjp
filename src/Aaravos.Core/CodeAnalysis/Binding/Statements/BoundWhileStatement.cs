using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Binding.Kinds;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Binding.Statements
{
    internal sealed class BoundWhileStatement : BoundLoopStatement
    {

        #region Properties

        public override BoundNodeKind Kind => BoundNodeKind.WhileStatement;
        public BoundExpression Condition { get; }
        public BoundStatement Body { get; }

        #endregion Properties

        #region Constructor

        public BoundWhileStatement(
            SyntaxNode syntax,
            BoundExpression condition,
            BoundStatement body,
            BoundLabel breakLabel,
            BoundLabel continueLabel
        ) : base(syntax, breakLabel, continueLabel)
        {
            Condition = condition;
            Body = body;
        }

        #endregion Constructor
    }
}
