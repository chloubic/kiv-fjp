using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Constants;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Aaravos.Core.CodeAnalysis
{
    internal static class BuiltinFunctions
    {

        #region Initializers

        public static readonly FunctionSymbol Print = new FunctionSymbol(KeywordConstants.FUNC_PRINT, ImmutableArray.Create(new ParameterSymbol("text", TypeSymbol.Any, 0)), TypeSymbol.Void);
        public static readonly FunctionSymbol PrintLine = new FunctionSymbol(KeywordConstants.FUNC_PRINT_LINE, ImmutableArray.Create(new ParameterSymbol("text", TypeSymbol.Any, 0)), TypeSymbol.Void);
        public static readonly FunctionSymbol Input = new FunctionSymbol(KeywordConstants.FUNC_INPUT, ImmutableArray<ParameterSymbol>.Empty, TypeSymbol.String);
        public static readonly FunctionSymbol Rnd = new FunctionSymbol(KeywordConstants.FUNC_RANDOM, ImmutableArray.Create(new ParameterSymbol("max", TypeSymbol.Int, 0)), TypeSymbol.Int);

        #endregion Initializers

        #region Methods

        internal static IEnumerable<FunctionSymbol> GetAll()
            => typeof(BuiltinFunctions).GetFields(BindingFlags.Public | BindingFlags.Static)
                                       .Where(f => f.FieldType == typeof(FunctionSymbol))
                                       .Select(f => (FunctionSymbol)f.GetValue(null)!);

        #endregion Methods

    }
}
