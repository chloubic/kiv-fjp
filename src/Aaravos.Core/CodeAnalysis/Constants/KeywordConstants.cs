using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Constants
{
    public class KeywordConstants
    {

        public const string FOR = "repeto";
        public const string WHILE = "donicum";
        public const string DO = "facitis";
        public const string TO = "ad";
        public const string BREAK = "ruptura";
        public const string CONTINUE = "pergo";
        public const string IF = "attack";
        public const string ELSE = "amen";
        public const string FALSE = "cruvus";
        public const string TRUE = "euge";
        public const string VAR = "variabilis";
        public const string CONST = "unus";
        public const string RETURN = "netario";
        public const string FUNCTION = "astrum";

        public const string FUNC_RANDOM = "Temere";
        public const string FUNC_INPUT = "Initus";
        public const string FUNC_PRINT = "Sculpo";
        public const string FUNC_PRINT_LINE = "Insculpo";
        public const string FUNC_MAIN = "Porta";

        public const string TYPE_ANY = "nihil";
        public const string TYPE_NUMBER = "numerus";
        public const string TYPE_STRING = "filum";
        public const string TYPE_BOOL = "verimas";
        public const string TYPE_VOID = "inane";
        public const string TYPE_ERROR = "?";

    }
}
