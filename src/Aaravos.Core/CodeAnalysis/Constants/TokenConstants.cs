using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Constants
{
    public class TokenConstants
    {

        public const char END_OF_STRING = '\0';
        public const char END_OF_LINE_R = '\r';
        public const char END_OF_LINE_N = '\n';
        public const char STAR = '*';
        public const char SLASH = '/';
        public const char PLUS = '+';
        public const char MINUS = '-';
        public const char EQUALITY = '=';
        public const char OPEN_ROUND_BRACKET = '(';
        public const char CLOSE_ROUND_BRACKET = ')';
        public const char OPEN_CURLY_BRACKET = '{';
        public const char CLOSE_CURLY_BRACKET = '}';
        public const char LESS_BRACKET = '<';
        public const char GREATER_BRACKET = '>';
        public const char COLON = ':';
        public const char SEMICOLON = ';';
        public const char COMMA = ',';
        public const char TILDE = '~';
        public const char HAT = '^';
        public const char AMPERSAND = '&';
        public const char PIPE = '|';
        public const char EXCLAMATION_MARK = '!';
        public const char UNDERSCORE = '_';
        public const char NUMBER_0 = '0';
        public const char NUMBER_1 = '1';
        public const char NUMBER_2 = '2';
        public const char NUMBER_3 = '3';
        public const char NUMBER_4 = '4';
        public const char NUMBER_5 = '5';
        public const char NUMBER_6 = '6';
        public const char NUMBER_7 = '7';
        public const char NUMBER_8 = '8';
        public const char NUMBER_9 = '9';

        public const string PLUS_EQUALS = "+=";
        public const string MINUS_EQUALS = "-=";
        public const string STAR_EQUALS = "*=";
        public const string SLASH_EQUALS = "/=";
        public const string LOGICAL_AND = "&&";
        public const string LOGICAL_AND_EQUALS = "&=";
        public const string LOGICAL_OR = "||";
        public const string LOGICAL_OR_EQUALS = "|=";
        public const string HAT_EQUALS = "^=";
        public const string DOUBLE_EQUALS = "==";
        public const string NOT_EQUALS = "!=";
        public const string LESS_EQUALS = "<=";
        public const string GREATER_EQUALS = ">=";

    }
}
