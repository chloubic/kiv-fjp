using Aaravos.Core.CodeAnalysis.Texting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Diagnosting
{
    public sealed class Diagnostic
    {

        #region Properties

        public bool IsError { get; }
        public TextLocation Location { get; }
        public string Message { get; }
        public bool IsWarning { get; }

        #endregion Properties

        #region Constructor

        private Diagnostic(
            bool isError,
            TextLocation location,
            string message
        )
        {
            IsError = isError;
            Location = location;
            Message = message;
            IsWarning = !IsError;
        }

        #endregion Constuctor

        #region Methods

        public override string ToString() => Message;

        public static Diagnostic Error(TextLocation location, string message)
            => new Diagnostic(isError: true, location, message);

        public static Diagnostic Warning(TextLocation location, string message)
            => new Diagnostic(isError: false, location, message);

        #endregion Methods

    }
}
