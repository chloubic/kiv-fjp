using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Constants;
using Aaravos.Core.CodeAnalysis.Texting;
using Mono.Cecil;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Diagnosting
{
    internal sealed class DiagnosticBag : IEnumerable<Diagnostic>
    {

        #region Properties

        private readonly List<Diagnostic> Diagnostics = new List<Diagnostic>();

        #endregion Properties

        #region Methods

        public IEnumerator<Diagnostic> GetEnumerator() => Diagnostics.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void AddRange(IEnumerable<Diagnostic> diagnostics)
            => Diagnostics.AddRange(diagnostics);

        private void ReportError(TextLocation location, string message)
            => Diagnostics.Add(Diagnostic.Error(location, message));

        private void ReportWarning(TextLocation location, string message)
            => Diagnostics.Add(Diagnostic.Warning(location, message));

        public void ReportInvalidNumber(TextLocation location, string text, TypeSymbol type)
            => ReportError(location, $"The number {text} isn't valid {type}.");

        public void ReportBadCharacter(TextLocation location, char character)
            => ReportError(location, $"Bad character input: '{character}'.");

        public void ReportUnterminatedString(TextLocation location)
            => ReportError(location, "Unterminated string literal.");

        public void ReportUnterminatedMultiLineComment(TextLocation location)
            => ReportError(location, "Unterminated multi-line comment.");

        public void ReportUnexpectedToken(TextLocation location, SyntaxKind actualKind, SyntaxKind expectedKind)
            => ReportError(location, $"Unexpected token <{actualKind}>, expected <{expectedKind}>.");

        public void ReportUndefinedUnaryOperator(TextLocation location, string operatorText, TypeSymbol operandType)
            => ReportError(location, $"Unary operator '{operatorText}' is not defined for type '{operandType}'.");

        public void ReportUndefinedBinaryOperator(TextLocation location, string operatorText, TypeSymbol leftType, TypeSymbol rightType)
            => ReportError(location, $"Binary operator '{operatorText}' is not defined for types '{leftType}' and '{rightType}'.");

        public void ReportParameterAlreadyDeclared(TextLocation location, string parameterName)
            => ReportError(location, $"A parameter with the name '{parameterName}' already exists.");

        public void ReportUndefinedVariable(TextLocation location, string name)
            => ReportError(location, $"Variable '{name}' doesn't exist.");

        public void ReportNotAVariable(TextLocation location, string name)
            => ReportError(location, $"'{name}' is not a variable.");

        public void ReportUndefinedType(TextLocation location, string name)
            => ReportError(location, $"Type '{name}' doesn't exist.");

        public void ReportCannotConvert(TextLocation location, TypeSymbol fromType, TypeSymbol toType)
            => ReportError(location, $"Cannot convert type '{fromType}' to '{toType}'.");

        public void ReportCannotConvertImplicitly(TextLocation location, TypeSymbol fromType, TypeSymbol toType)
            => ReportError(location, $"Cannot convert type '{fromType}' to '{toType}'. An explicit conversion exists (are you missing a cast?)");

        public void ReportSymbolAlreadyDeclared(TextLocation location, string name)
            => ReportError(location, $"'{name}' is already declared.");

        public void ReportCannotAssign(TextLocation location, string name)
            => ReportError(location, $"Variable '{name}' is read-only and cannot be assigned to.");

        public void ReportUndefinedFunction(TextLocation location, string name)
            => ReportError(location, $"Function '{name}' doesn't exist.");

        public void ReportNotAFunction(TextLocation location, string name)
            => ReportError(location, $"'{name}' is not a function.");

        public void ReportWrongArgumentCount(TextLocation location, string name, int expectedCount, int actualCount)
            => ReportError(location, $"Function '{name}' requires {expectedCount} arguments but was given {actualCount}.");

        public void ReportExpressionMustHaveValue(TextLocation location)
            => ReportError(location, "Expression must have a value.");

        public void ReportInvalidBreakOrContinue(TextLocation location, string text)
            => ReportError(location, $"The keyword '{text}' can only be used inside of loops.");

        public void ReportAllPathsMustReturn(TextLocation location)
            => ReportError(location, "Not all code paths return a value.");

        public void ReportInvalidReturnExpression(TextLocation location, string functionName)
            => ReportError(location, $"Since the function '{functionName}' does not return a value the '{KeywordConstants.RETURN}' keyword cannot be followed by an expression.");

        public void ReportInvalidReturnWithValueInGlobalStatements(TextLocation location)
            => ReportError(location, $"The '{KeywordConstants.RETURN}' keyword cannot be followed by an expression in global statements.");

        public void ReportMissingReturnExpression(TextLocation location, TypeSymbol returnType)
            => ReportError(location, $"An expression of type '{returnType}' is expected.");

        public void ReportInvalidExpressionStatement(TextLocation location)
            => ReportError(location, $"Only assignment and call expressions can be used as a statement.");

        public void ReportOnlyOneFileCanHaveGlobalStatements(TextLocation location)
            => ReportError(location, $"At most one file can have global statements.");

        public void ReportMainMustHaveCorrectSignature(TextLocation location)
            => ReportError(location, $"{KeywordConstants.FUNC_MAIN} must not take arguments and not return anything.");

        public void ReportCannotMixMainAndGlobalStatements(TextLocation location)
            => ReportError(location, $"Cannot declare {KeywordConstants.FUNC_MAIN} function when global statements are used.");

        public void ReportInvalidReference(string path)
            => ReportError(default, $"The reference is not a valid .NET assembly: '{path}'.");

        public void ReportRequiredTypeNotFound(string? aaravosName, string metadataName)
        {
            var message = aaravosName == null
                ? $"The required type '{metadataName}' cannot be resolved among the given references."
                : $"The required type '{aaravosName}' ('{metadataName}') cannot be resolved among the given references.";
            ReportError(default, message);
        }

        public void ReportRequiredTypeAmbiguous(string? aaravosName, string metadataName, TypeDefinition[] foundTypes)
        {
            var assemblyNames = foundTypes.Select(t => t.Module.Assembly.Name.Name);
            var assemblyNameList = string.Join(", ", assemblyNames);
            var message = aaravosName == null
                ? $"The required type '{metadataName}' was found in multiple references: {assemblyNameList}."
                : $"The required type '{aaravosName}' ('{metadataName}') was found in multiple references: {assemblyNameList}.";
            ReportError(default, message);
        }

        public void ReportRequiredMethodNotFound(string typeName, string methodName, string[] parameterTypeNames)
        {
            var parameterTypeNameList = string.Join(", ", parameterTypeNames);
            var message = $"The required method '{typeName}.{methodName}({parameterTypeNameList})' cannot be resolved among the given references.";
            ReportError(default, message);
        }

        public void ReportUnreachableCode(TextLocation location)
            => ReportWarning(location, $"Unreachable code detected.");

        public void ReportUnreachableCode(SyntaxNode node)
        {
            switch (node.Kind)
            {
                case SyntaxKind.BlockStatement:
                    var firstStatement = ((BlockStatementSyntax)node).Statements.FirstOrDefault();
                    // Report just for non empty blocks.
                    if (firstStatement != null)
                        ReportUnreachableCode(firstStatement);
                    return;
                case SyntaxKind.VariableDeclaration:
                    ReportUnreachableCode(((VariableDeclarationSyntax)node).Keyword.Location);
                    return;
                case SyntaxKind.IfStatement:
                    ReportUnreachableCode(((IfStatementSyntax)node).IfKeyword.Location);
                    return;
                case SyntaxKind.WhileStatement:
                    ReportUnreachableCode(((WhileStatementSyntax)node).WhileKeyword.Location);
                    return;
                case SyntaxKind.DoWhileStatement:
                    ReportUnreachableCode(((DoWhileStatementSyntax)node).DoKeyword.Location);
                    return;
                case SyntaxKind.ForStatement:
                    ReportUnreachableCode(((ForStatementSyntax)node).Keyword.Location);
                    return;
                case SyntaxKind.BreakStatement:
                    ReportUnreachableCode(((BreakStatementSyntax)node).Keyword.Location);
                    return;
                case SyntaxKind.ContinueStatement:
                    ReportUnreachableCode(((ContinueStatementSyntax)node).Keyword.Location);
                    return;
                case SyntaxKind.ReturnStatement:
                    ReportUnreachableCode(((ReturnStatementSyntax)node).ReturnKeyword.Location);
                    return;
                case SyntaxKind.ExpressionStatement:
                    var expression = ((ExpressionStatementSyntax)node).Expression;
                    ReportUnreachableCode(expression);
                    return;
                case SyntaxKind.CallExpression:
                    ReportUnreachableCode(((CallExpressionSyntax)node).Identifier.Location);
                    return;
                default:
                    throw new Exception($"Unexpected syntax {node.Kind}");
            }
        }

        #endregion Methods

    }
}
