using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Aaravos.Core.CodeAnalysis.Diagnosting
{
    public static class DiagnosticExtensions
    {

        #region Methods

        public static bool HasErrors(this ImmutableArray<Diagnostic> diagnostics)
            => diagnostics.Any(d => d.IsError);

        public static bool HasErrors(this IEnumerable<Diagnostic> diagnostics)
            => diagnostics.Any(d => d.IsError);

        #endregion Methods

    }
}
