using Aaravos.Core.CodeAnalysis.Diagnosting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Evaluating
{
    public sealed class EvaluationResult
    {

        #region Properties

        public ImmutableArray<Diagnostic> Diagnostics { get; }
        public object? Value { get; }

        #endregion Properties

        #region Constructor

        public EvaluationResult(
            ImmutableArray<Diagnostic> diagnostics,
            object? value
        )
        {
            Diagnostics = diagnostics;
            Value = value;
        }

        #endregion Constructor

    }
}
