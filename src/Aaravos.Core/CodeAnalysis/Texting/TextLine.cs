using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Texting
{
    public sealed class TextLine
    {

        #region Properties

        public SourceText Text { get; }
        public int Start { get; }
        public int Length { get; }
        public int End => Start + Length;
        public int LengthIncludingLineBreak { get; }
        public TextSpan Span => new TextSpan(Start, Length);
        public TextSpan SpanIncludingLineBreak => new TextSpan(Start, LengthIncludingLineBreak);

        #endregion Properties

        #region Constructor

        public TextLine(
            SourceText text,
            int start,
            int length,
            int lengthIncludingLineBreak
        )
        {
            Text = text;
            Start = start;
            Length = length;
            LengthIncludingLineBreak = lengthIncludingLineBreak;
        }

        #endregion Constructor

        #region Methods
        public override string ToString() => Text.ToString(Span);

        #endregion Methods
    }
}
