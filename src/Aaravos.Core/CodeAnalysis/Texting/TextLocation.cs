using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Texting
{
    public struct TextLocation
    {

        #region Properties

        public SourceText Text { get; }
        public TextSpan Span { get; }

        public string FileName => Text.FileName;
        public int StartLine => Text.GetLineIndex(Span.Start);
        public int StartCharacter => Span.Start - Text.Lines[StartLine].Start;
        public int EndLine => Text.GetLineIndex(Span.End);
        public int EndCharacter => Span.End - Text.Lines[EndLine].Start;

        #endregion Properties

        #region Constructor

        public TextLocation(
            SourceText text,
            TextSpan span
        )
        {
            Text = text;
            Span = span;
        }

        #endregion Constructor

    }
}
