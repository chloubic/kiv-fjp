using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Core.CodeAnalysis.Texting
{
    public struct TextSpan
    {

        #region Properties

        public int Start { get; }
        public int Length { get; }
        public int End => Start + Length;

        #endregion Properties

        #region Constructor

        public TextSpan(
            int start,
            int length
        )
        {
            Start = start;
            Length = length;
        }

        #endregion Constructor

        #region Methods

        public static TextSpan FromBounds(int start, int end)
        {
            var length = end - start;
            return new TextSpan(start, length);
        }

        public bool OverlapsWith(TextSpan span)
        {
            return Start < span.End &&
                   End > span.Start;
        }

        public override string ToString() => $"{Start}..{End}";

        #endregion Methods
    }
}
