using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Aaravos.Authoring;
using Aaravos.Core.CodeAnalysis;
using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Diagnosting;
using Aaravos.Core.IO;

namespace Aaravos.REPL
{

    /// <summary>
    /// Aaravos's implementation of REPL
    /// </summary>
    internal sealed class AaravosRepl : Repl
    {

        #region Properties

        /// <summary>
        /// Indicates submissions loading
        /// </summary>
        private bool LoadingSubmission;

        /// <summary>
        /// Creates empty compilation
        /// </summary>
        private static readonly Compilation EmptyCompilation = Compilation.CreateScript(null);

        /// <summary>
        /// Previous compilation property
        /// </summary>
        private Compilation? Previous;

        /// <summary>
        /// Indicates showing parse tree
        /// </summary>
        private bool ShowTree;

        /// <summary>
        /// Indicates showing program tree
        /// </summary>
        private bool ShowProgram;

        /// <summary>
        /// Contains declared variables in REPL
        /// </summary>
        private readonly Dictionary<VariableSymbol, object> Variables = new Dictionary<VariableSymbol, object>();

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Aaravos REPL constructor initializes submissions
        /// </summary>
        public AaravosRepl() => LoadSubmissions();

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Renders line to command-line
        /// </summary>
        /// <param name="lines">Word to render</param>
        /// <param name="lineIndex">Index of line to render</param>
        /// <param name="state">Event state</param>
        /// <returns></returns>
        protected override object? RenderLine(IReadOnlyList<string> lines, int lineIndex, object? state)
        {
            SyntaxTree syntaxTree;

            if (state == null)
            {
                var text = string.Join(Environment.NewLine, lines);
                syntaxTree = SyntaxTree.Parse(text);
            }
            else
            {
                syntaxTree = (SyntaxTree)state;
            }

            var lineSpan = syntaxTree.Text.Lines[lineIndex].Span;
            var classifiedSpans = Classifier.Classify(syntaxTree, lineSpan);

            foreach (var classifiedSpan in classifiedSpans)
            {
                var classifiedText = syntaxTree.Text.ToString(classifiedSpan.Span);

                switch (classifiedSpan.Classification)
                {
                    case Classification.Keyword:
                        Console.ForegroundColor = ConsoleColor.Blue;
                        break;
                    case Classification.Identifier:
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        break;
                    case Classification.Number:
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        break;
                    case Classification.String:
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        break;
                    case Classification.Comment:
                        Console.ForegroundColor = ConsoleColor.Green;
                        break;
                    case Classification.Text:
                    default:
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        break;
                }

                Console.Write(classifiedText);
                Console.ResetColor();
            }

            return syntaxTree;
        }

        /// <summary>
        /// Handles exit command
        /// </summary>
        [MetaCommand("exit", "Exits the REPL")]
        private void EvaluateExit() => Environment.Exit(0);

        /// <summary>
        /// Handles cls command
        /// </summary>
        [MetaCommand("cls", "Clears the screen")]
        private void EvaluateCls() => Console.Clear();

        /// <summary>
        /// Handles reset command
        /// </summary>
        [MetaCommand("reset", "Clears all previous submissions")]
        private void EvaluateReset()
        {
            Previous = null;
            Variables.Clear();
            ClearSubmissions();
        }

        /// <summary>
        /// Handles show parse tree command
        /// </summary>
        [MetaCommand("showTree", "Shows the parse tree")]
        private void EvaluateShowTree()
        {
            ShowTree = !ShowTree;
            Console.WriteLine(ShowTree ? "Showing parse trees." : "Not showing parse trees.");
        }

        /// <summary>
        /// Handles show program tree command
        /// </summary>
        [MetaCommand("showProgram", "Shows the bound tree")]
        private void EvaluateShowProgram()
        {
            ShowProgram = !ShowProgram;
            Console.WriteLine(ShowProgram ? "Showing bound tree." : "Not showing bound tree.");
        }

        /// <summary>
        /// Handles load command
        /// </summary>
        [MetaCommand("load", "Loads a script file")]
        private void EvaluateLoad(string path)
        {
            path = Path.GetFullPath(path);

            if (!File.Exists(path))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"error: file does not exist '{path}'");
                Console.ResetColor();
                return;
            }

            var text = File.ReadAllText(path);
            EvaluateSubmission(text);
        }

        /// <summary>
        /// Handles ls command
        /// </summary>
        [MetaCommand("ls", "Lists all symbols")]
        private void EvaluateLs()
        {
            var compilation = Previous ?? EmptyCompilation;
            var symbols = compilation.GetSymbols().OrderBy(s => s.Kind).ThenBy(s => s.Name);
            foreach (var symbol in symbols)
            {
                symbol.WriteTo(Console.Out);
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Handles dump command
        /// </summary>
        [MetaCommand("dump", "Shows bound tree of a given function")]
        private void EvaluateDump(string functionName)
        {
            var compilation = Previous ?? EmptyCompilation;
            var symbol = compilation.GetSymbols().OfType<FunctionSymbol>().SingleOrDefault(f => f.Name == functionName);
            if (symbol == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"error: function '{functionName}' does not exist");
                Console.ResetColor();
                return;
            }

            compilation.EmitTree(symbol, Console.Out);
        }

        /// <summary>
        /// Checks if submission is complete
        /// </summary>
        /// <param name="text">User submission</param>
        /// <returns>If submission is complete</returns>
        protected override bool IsCompleteSubmission(string text)
        {
            if (string.IsNullOrEmpty(text))
                return true;

            var lastTwoLinesAreBlank = text.Split(Environment.NewLine)
                                           .Reverse()
                                           .TakeWhile(s => string.IsNullOrEmpty(s))
                                           .Take(2)
                                           .Count() == 2;
            if (lastTwoLinesAreBlank)
                return true;

            var syntaxTree = SyntaxTree.Parse(text);

            // Use Members because we need to exclude the EndOfFileToken.
            var lastMember = syntaxTree.Root.Members.LastOrDefault();
            if (lastMember == null || lastMember.GetLastToken().IsMissing)
                return false;

            return true;
        }

        /// <summary>
        /// Evaluates submission
        /// </summary>
        /// <param name="text">User submission</param>
        protected override void EvaluateSubmission(string text)
        {
            var syntaxTree = SyntaxTree.Parse(text);
            var compilation = Compilation.CreateScript(Previous, syntaxTree);

            if (ShowTree)
                syntaxTree.Root.WriteTo(Console.Out);

            if (ShowProgram)
                compilation.EmitTree(Console.Out);

            var result = compilation.Evaluate(Variables);
            Console.Out.WriteDiagnostics(result.Diagnostics);

            if (!result.Diagnostics.HasErrors())
            {
                if (result.Value != null)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(result.Value);
                    Console.ResetColor();
                }
                Previous = compilation;

                SaveSubmission(text);
            }
        }

        /// <summary>
        /// Returns path of submission directory
        /// </summary>
        /// <returns>Path of submission directory</returns>
        private static string GetSubmissionsDirectory()
        {
            var localAppData = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var submissionsDirectory = Path.Combine(localAppData, "Aaravos", "Submissions");
            return submissionsDirectory;
        }

        /// <summary>
        /// Loads saved submissions
        /// </summary>
        private void LoadSubmissions()
        {
            var submissionsDirectory = GetSubmissionsDirectory();
            if (!Directory.Exists(submissionsDirectory))
                return;

            var files = Directory.GetFiles(submissionsDirectory).OrderBy(f => f).ToArray();
            if (files.Length == 0)
                return;

            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine($"Loaded {files.Length} submission(s)");
            Console.ResetColor();

            LoadingSubmission = true;

            foreach (var file in files)
            {
                var text = File.ReadAllText(file);
                EvaluateSubmission(text);
            }

            LoadingSubmission = false;
        }

        /// <summary>
        /// Clears saved submissions
        /// </summary>
        private static void ClearSubmissions()
        {
            var dir = GetSubmissionsDirectory();
            if (Directory.Exists(dir))
                Directory.Delete(dir, recursive: true);
        }

        /// <summary>
        /// Saves submission
        /// </summary>
        /// <param name="text">Text to save</param>
        private void SaveSubmission(string text)
        {
            if (LoadingSubmission)
                return;

            var submissionsDirectory = GetSubmissionsDirectory();
            Directory.CreateDirectory(submissionsDirectory);
            var count = Directory.GetFiles(submissionsDirectory).Length;
            var name = $"submission{count:0000}";
            var fileName = Path.Combine(submissionsDirectory, name);
            File.WriteAllText(fileName, text);
        }

        #endregion Methods

    }
}