using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Authoring
{
    /// <summary>
    /// Classification categories
    /// </summary>
    public enum Classification
    {
        Text,
        Keyword,
        Identifier,
        Number,
        String,
        Comment
    }
}
