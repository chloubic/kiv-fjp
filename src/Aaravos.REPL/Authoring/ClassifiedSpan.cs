using Aaravos.Core.CodeAnalysis.Texting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aaravos.Authoring
{

    /// <summary>
    /// Classified span class
    /// </summary>
    public sealed class ClassifiedSpan
    {

        #region Properties

        /// <summary>
        /// Span instance property
        /// </summary>
        public TextSpan Span { get; }

        /// <summary>
        /// Classification category property
        /// </summary>
        public Classification Classification { get; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Classified span constructor
        /// </summary>
        /// <param name="span">Span instance</param>
        /// <param name="classification">Classification category</param>
        public ClassifiedSpan(
            TextSpan span,
            Classification classification
        )
        {
            Span = span;
            Classification = classification;
        }

        #endregion Constructor

    }
}
