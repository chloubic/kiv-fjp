namespace Aaravos.REPL
{

    /// <summary>
    /// Entry point of RELP application
    /// </summary>
    internal static class Program
    {

        #region Methods

        /// <summary>
        /// Initializes and runs Read-Eval-Print-Loop application
        /// </summary>
        private static void Main()
        {
            var repl = new AaravosRepl();
            repl.Run();
        }

        #endregion Methods

    }
}
