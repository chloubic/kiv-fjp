using Aaravos.Core.IO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Aaravos.REPL
{

    /// <summary>
    /// Abstract <c>Repl</c> with common feature implementation
    /// </summary>
    internal abstract class Repl
    {

        #region Properties

        /// <summary>
        /// List of possible meta commands
        /// </summary>
        private readonly List<MetaCommand> MetaCommands = new List<MetaCommand>();

        /// <summary>
        /// Submission history for used commands in RELP
        /// </summary>
        private readonly List<string> SubmissionHistory = new List<string>();

        /// <summary>
        /// Actual index in <c>SubmissionHistory</c> list
        /// </summary>
        private int SubmissionHistoryIndex;

        /// <summary>
        /// Signalizes done rutine
        /// </summary>
        private bool Done;

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Calls methods for meta commands initialization
        /// </summary>
        protected Repl() => InitializeMetaCommands();

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Initializes possible meta commands
        /// </summary>
        private void InitializeMetaCommands()
        {
            var methods = GetType().GetMethods(BindingFlags.Public |
                                               BindingFlags.NonPublic |
                                               BindingFlags.Static |
                                               BindingFlags.Instance |
                                               BindingFlags.FlattenHierarchy);
            foreach (var method in methods)
            {
                var attribute = method.GetCustomAttribute<MetaCommandAttribute>();
                if (attribute == null)
                    continue;

                var metaCommand = new MetaCommand(attribute.Name, attribute.Description, method);
                MetaCommands.Add(metaCommand);
            }
        }

        /// <summary>
        /// Runs infinite R-E-P-Loop
        /// </summary>
        public void Run()
        {
            while (true)
            {
                var text = EditSubmission();
                if (string.IsNullOrEmpty(text))
                    continue;

                if (!text.Contains(Environment.NewLine) && text.StartsWith("#"))
                    EvaluateMetaCommand(text);
                else
                    EvaluateSubmission(text);

                SubmissionHistory.Add(text);
                SubmissionHistoryIndex = 0;
            }
        }

        #endregion Methods

        #region Delegates

        /// <summary>
        /// Line renderer handler
        /// </summary>
        /// <param name="lines">List with lines content</param>
        /// <param name="lineIndex">Actual line index</param>
        /// <param name="state">Handler state</param>
        /// <returns></returns>
        private delegate object? LineRenderHandler(IReadOnlyList<string> lines, int lineIndex, object? state);

        #endregion Delegates

        #region Inner classes

        /// <summary>
        /// Contains information about subsission view
        /// </summary>
        private sealed class SubmissionView
        {

            #region Properties

            /// <summary>
            /// Line renderer
            /// </summary>
            private readonly LineRenderHandler LineRenderer;

            /// <summary>
            /// Actual document submission
            /// </summary>
            private readonly ObservableCollection<string> SubmissionDocument;

            /// <summary>
            /// Cursor index
            /// </summary>
            private int CursorTop;

            /// <summary>
            /// Actual rendered lines counter
            /// </summary>
            private int RenderedLineCount;

            /// <summary>
            /// Current line index
            /// </summary>
            private int CurrentLineProperty;

            /// <summary>
            /// Current character in line index
            /// </summary>
            private int CurrentCharacterProperty;

            #endregion Properties

            #region Constructor

            /// <summary>
            /// Constructor of <c>SubmissionView</c>
            /// </summary>
            /// <param name="lineRenderer">Line renderer handler</param>
            /// <param name="submissionDocument">Instance of list with submission content</param>
            public SubmissionView(
                LineRenderHandler lineRenderer,
                ObservableCollection<string> submissionDocument
            )
            {
                LineRenderer = lineRenderer;
                SubmissionDocument = submissionDocument;
                SubmissionDocument.CollectionChanged += SubmissionDocumentChanged;
                CursorTop = Console.CursorTop;
                Render();
            }

            #endregion Constructor

            #region Methods

            /// <summary>
            /// Handles document change
            /// </summary>
            /// <param name="sender">Event sender reference</param>
            /// <param name="e">Event arguments</param>
            private void SubmissionDocumentChanged(object sender, NotifyCollectionChangedEventArgs e) => Render();

            /// <summary>
            /// Renderer method
            /// </summary>
            private void Render()
            {
                Console.CursorVisible = false;

                var lineCount = 0;
                var state = (object?)null;

                foreach (var line in SubmissionDocument)
                {
                    if (CursorTop + lineCount >= Console.WindowHeight)
                    {
                        Console.SetCursorPosition(0, Console.WindowHeight - 1);
                        Console.WriteLine();
                        if (CursorTop > 0)
                            CursorTop--;
                    }

                    Console.SetCursorPosition(0, CursorTop + lineCount);
                    Console.ForegroundColor = ConsoleColor.Green;

                    if (lineCount == 0)
                        Console.Write("» ");
                    else
                        Console.Write("· ");

                    Console.ResetColor();
                    state = LineRenderer(SubmissionDocument, lineCount, state);
                    Console.Write(new string(' ', Console.WindowWidth - line.Length - 2));
                    lineCount++;
                }

                var numberOfBlankLines = RenderedLineCount - lineCount;
                if (numberOfBlankLines > 0)
                {
                    var blankLine = new string(' ', Console.WindowWidth);
                    for (var i = 0; i < numberOfBlankLines; i++)
                    {
                        Console.SetCursorPosition(0, CursorTop + lineCount + i);
                        Console.WriteLine(blankLine);
                    }
                }

                RenderedLineCount = lineCount;

                Console.CursorVisible = true;
                UpdateCursorPosition();
            }

            /// <summary>
            /// Handles cursor position update
            /// </summary>
            private void UpdateCursorPosition()
            {
                Console.CursorTop = CursorTop + CurrentLine;
                Console.CursorLeft = 2 + CurrentCharacter;
            }

            /// <summary>
            /// Checker for <c>CurrentLineProperty</c>
            /// </summary>
            public int CurrentLine
            {
                get => CurrentLineProperty;
                set
                {
                    if (CurrentLine != value)
                    {
                        CurrentLineProperty = value;
                        CurrentCharacter = Math.Min(SubmissionDocument[CurrentLine].Length, CurrentCharacter);

                        UpdateCursorPosition();
                    }
                }
            }

            /// <summary>
            /// Checker for <c>CurrentCharacterProperty</c>
            /// </summary>
            public int CurrentCharacter
            {
                get => CurrentCharacterProperty;
                set
                {
                    if (CurrentCharacter != value)
                    {
                        CurrentCharacterProperty = value;
                        UpdateCursorPosition();
                    }
                }
            }

            #endregion Methods

        }

        /// <summary>
        /// Represents command-line attributes
        /// </summary>
        [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
        protected sealed class MetaCommandAttribute : Attribute
        {

            #region Constructor

            /// <summary>
            /// Command attribute constructor
            /// </summary>
            /// <param name="name">Attribute name</param>
            /// <param name="description">Attribute description</param>
            public MetaCommandAttribute(
                string name,
                string description
            )
            {
                Name = name;
                Description = description;
            }

            #endregion Constructor

            #region Properties

            /// <summary>
            /// Attribute name property
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Attribute description property
            /// </summary>
            public string Description { get; }

            #endregion Properties

        }

        /// <summary>
        /// Represents command
        /// </summary>
        private sealed class MetaCommand
        {

            #region Constructor

            /// <summary>
            /// Command constructor
            /// </summary>
            /// <param name="name">Command name</param>
            /// <param name="description">Command description</param>
            /// <param name="method">Method to call</param>
            public MetaCommand(
                string name,
                string description,
                MethodInfo method
            )
            {
                Name = name;
                Description = description;
                Method = method;
            }

            #endregion Constructor

            #region Properties

            /// <summary>
            /// Command name property
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Command description property
            /// </summary>
            public string Description { get; }

            /// <summary>
            /// Method to call property
            /// </summary>
            public MethodInfo Method { get; }

            #endregion Properties

        }

        #endregion Inner classes

        #region Methods

        /// <summary>
        /// Edits submission
        /// </summary>
        /// <returns>Joined document</returns>
        private string EditSubmission()
        {
            Done = false;

            var document = new ObservableCollection<string>() { "" };
            var view = new SubmissionView(RenderLine, document);

            while (!Done)
            {
                var key = Console.ReadKey(true);
                HandleKey(key, document, view);
            }

            view.CurrentLine = document.Count - 1;
            view.CurrentCharacter = document[view.CurrentLine].Length;
            Console.WriteLine();

            return string.Join(Environment.NewLine, document);
        }

        /// <summary>
        /// Command line key handlers
        /// </summary>
        /// <param name="key">Clicked key</param>
        /// <param name="document">Document instance</param>
        /// <param name="view">View instance</param>
        private void HandleKey(ConsoleKeyInfo key, ObservableCollection<string> document, SubmissionView view)
        {
            if (key.Modifiers == default(ConsoleModifiers))
            {
                switch (key.Key)
                {
                    case ConsoleKey.Escape:
                        HandleEscape(document, view);
                        break;
                    case ConsoleKey.Enter:
                        HandleEnter(document, view);
                        break;
                    case ConsoleKey.LeftArrow:
                        HandleLeftArrow(document, view);
                        break;
                    case ConsoleKey.RightArrow:
                        HandleRightArrow(document, view);
                        break;
                    case ConsoleKey.UpArrow:
                        HandleUpArrow(document, view);
                        break;
                    case ConsoleKey.DownArrow:
                        HandleDownArrow(document, view);
                        break;
                    case ConsoleKey.Backspace:
                        HandleBackspace(document, view);
                        break;
                    case ConsoleKey.Delete:
                        HandleDelete(document, view);
                        break;
                    case ConsoleKey.Home:
                        HandleHome(document, view);
                        break;
                    case ConsoleKey.End:
                        HandleEnd(document, view);
                        break;
                    case ConsoleKey.Tab:
                        HandleTab(document, view);
                        break;
                    case ConsoleKey.PageUp:
                        HandlePageUp(document, view);
                        break;
                    case ConsoleKey.PageDown:
                        HandlePageDown(document, view);
                        break;
                }
            }
            else if (key.Modifiers == ConsoleModifiers.Control)
            {
                switch (key.Key)
                {
                    case ConsoleKey.Enter:
                        HandleControlEnter(document, view);
                        break;
                }
            }

            if (key.Key != ConsoleKey.Backspace && key.KeyChar >= ' ')
                HandleTyping(document, view, key.KeyChar.ToString());
        }

        /// <summary>
        /// Method handles escape key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleEscape(ObservableCollection<string> document, SubmissionView view)
        {
            document.Clear();
            document.Add(string.Empty);
            view.CurrentLine = 0;
            view.CurrentCharacter = 0;
        }

        /// <summary>
        /// Method handles enter key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleEnter(ObservableCollection<string> document, SubmissionView view)
        {
            var submissionText = string.Join(Environment.NewLine, document);
            if (submissionText.StartsWith("#") || IsCompleteSubmission(submissionText))
            {
                Done = true;
                return;
            }

            InsertLine(document, view);
        }

        /// <summary>
        /// Method handles control + enter key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleControlEnter(ObservableCollection<string> document, SubmissionView view) => InsertLine(document, view);

        /// <summary>
        /// Inserts line into document
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private static void InsertLine(ObservableCollection<string> document, SubmissionView view)
        {
            var remainder = document[view.CurrentLine].Substring(view.CurrentCharacter);
            document[view.CurrentLine] = document[view.CurrentLine].Substring(0, view.CurrentCharacter);

            var lineIndex = view.CurrentLine + 1;
            document.Insert(lineIndex, remainder);
            view.CurrentCharacter = 0;
            view.CurrentLine = lineIndex;
        }

        /// <summary>
        /// Method handles left arrow key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleLeftArrow(ObservableCollection<string> document, SubmissionView view)
        {
            if (view.CurrentCharacter > 0)
                view.CurrentCharacter--;
        }

        /// <summary>
        /// Method handles right arrow key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleRightArrow(ObservableCollection<string> document, SubmissionView view)
        {
            var line = document[view.CurrentLine];
            if (view.CurrentCharacter <= line.Length - 1)
                view.CurrentCharacter++;
        }

        /// <summary>
        /// Method handles up arrow key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleUpArrow(ObservableCollection<string> document, SubmissionView view)
        {
            if (view.CurrentLine > 0)
                view.CurrentLine--;
        }

        /// <summary>
        /// Method handles down arrow key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleDownArrow(ObservableCollection<string> document, SubmissionView view)
        {
            if (view.CurrentLine < document.Count - 1)
                view.CurrentLine++;
        }

        /// <summary>
        /// Method handles backspace key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleBackspace(ObservableCollection<string> document, SubmissionView view)
        {
            var start = view.CurrentCharacter;
            if (start == 0)
            {
                if (view.CurrentLine == 0)
                    return;

                var currentLine = document[view.CurrentLine];
                var previousLine = document[view.CurrentLine - 1];
                document.RemoveAt(view.CurrentLine);
                view.CurrentLine--;
                document[view.CurrentLine] = previousLine + currentLine;
                view.CurrentCharacter = previousLine.Length;
            }
            else
            {
                var lineIndex = view.CurrentLine;
                var line = document[lineIndex];
                var before = line.Substring(0, start - 1);
                var after = line.Substring(start);
                document[lineIndex] = before + after;
                view.CurrentCharacter--;
            }
        }

        /// <summary>
        /// Method handles delete key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleDelete(ObservableCollection<string> document, SubmissionView view)
        {
            var lineIndex = view.CurrentLine;
            var line = document[lineIndex];
            var start = view.CurrentCharacter;
            if (start >= line.Length)
            {
                if (view.CurrentLine == document.Count - 1)
                {
                    return;
                }

                var nextLine = document[view.CurrentLine + 1];
                document[view.CurrentLine] += nextLine;
                document.RemoveAt(view.CurrentLine + 1);
                return;
            }

            var before = line.Substring(0, start);
            var after = line.Substring(start + 1);
            document[lineIndex] = before + after;
        }

        /// <summary>
        /// Method handles home key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleHome(ObservableCollection<string> document, SubmissionView view)
            => view.CurrentCharacter = 0;

        /// <summary>
        /// Method handles end key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleEnd(ObservableCollection<string> document, SubmissionView view)
            => view.CurrentCharacter = document[view.CurrentLine].Length;

        /// <summary>
        /// Method handles tab key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandleTab(ObservableCollection<string> document, SubmissionView view)
        {
            const int TabWidth = 4;
            var start = view.CurrentCharacter;
            var remainingSpaces = TabWidth - start % TabWidth;
            var line = document[view.CurrentLine];
            document[view.CurrentLine] = line.Insert(start, new string(' ', remainingSpaces));
            view.CurrentCharacter += remainingSpaces;
        }

        /// <summary>
        /// Method handles page up key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandlePageUp(ObservableCollection<string> document, SubmissionView view)
        {
            SubmissionHistoryIndex--;
            if (SubmissionHistoryIndex < 0)
                SubmissionHistoryIndex = SubmissionHistory.Count - 1;
            UpdateDocumentFromHistory(document, view);
        }

        /// <summary>
        /// Method handles page down key
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void HandlePageDown(ObservableCollection<string> document, SubmissionView view)
        {
            SubmissionHistoryIndex++;
            if (SubmissionHistoryIndex > SubmissionHistory.Count - 1)
                SubmissionHistoryIndex = 0;
            UpdateDocumentFromHistory(document, view);
        }

        /// <summary>
        /// Updates document from history
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        private void UpdateDocumentFromHistory(ObservableCollection<string> document, SubmissionView view)
        {
            if (SubmissionHistory.Count == 0)
                return;

            document.Clear();

            var historyItem = SubmissionHistory[SubmissionHistoryIndex];
            var lines = historyItem.Split(Environment.NewLine);
            foreach (var line in lines)
                document.Add(line);

            view.CurrentLine = document.Count - 1;
            view.CurrentCharacter = document[view.CurrentLine].Length;
        }

        /// <summary>
        /// Method handles user typing
        /// </summary>
        /// <param name="document">Current document</param>
        /// <param name="view">Current view</param>
        /// <param name="text">Text to handle</param>
        private void HandleTyping(ObservableCollection<string> document, SubmissionView view, string text)
        {
            var lineIndex = view.CurrentLine;
            var start = view.CurrentCharacter;
            document[lineIndex] = document[lineIndex].Insert(start, text);
            view.CurrentCharacter += text.Length;
        }

        /// <summary>
        /// Method clears history
        /// </summary>
        protected void ClearHistory() => SubmissionHistory.Clear();

        /// <summary>
        /// Renders line to command-line
        /// </summary>
        /// <param name="lines">Lines list</param>
        /// <param name="lineIndex">Line index to render</param>
        /// <param name="state">Event state</param>
        /// <returns></returns>
        protected virtual object? RenderLine(IReadOnlyList<string> lines, int lineIndex, object? state)
        {
            Console.Write(lines[lineIndex]);
            return state;
        }

        /// <summary>
        /// Evaluates user command
        /// </summary>
        /// <param name="input">User text input</param>
        private void EvaluateMetaCommand(string input)
        {
            // Parse arguments

            var args = new List<string>();
            var inQuotes = false;
            var position = 1;
            var sb = new StringBuilder();
            while (position < input.Length)
            {
                var c = input[position];
                var l = position + 1 >= input.Length ? '\0' : input[position + 1];

                if (char.IsWhiteSpace(c))
                {
                    if (!inQuotes)
                        CommitPendingArgument();
                    else
                        sb.Append(c);
                }
                else if (c == '\"')
                {
                    if (!inQuotes)
                        inQuotes = true;
                    else if (l == '\"')
                    {
                        sb.Append(c);
                        position++;
                    }
                    else
                        inQuotes = false;
                }
                else
                {
                    sb.Append(c);
                }

                position++;
            }

            CommitPendingArgument();

            void CommitPendingArgument()
            {
                var arg = sb.ToString();
                if (!string.IsNullOrWhiteSpace(arg))
                    args.Add(arg);
                sb.Clear();
            }

            var commandName = args.FirstOrDefault();
            if (args.Count > 0)
                args.RemoveAt(0);

            var command = MetaCommands.SingleOrDefault(mc => mc.Name == commandName);
            if (command == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Invalid command {input}.");
                Console.ResetColor();
                return;
            }

            var parameters = command.Method.GetParameters();

            if (args.Count != parameters.Length)
            {
                var parameterNames = string.Join(" ", parameters.Select(p => $"<{p.Name}>"));
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"error: invalid number of arguments");
                Console.WriteLine($"usage: #{command.Name} {parameterNames}");
                Console.ResetColor();
                return;
            }

            var instance = command.Method.IsStatic ? null : this;
            command.Method.Invoke(instance, args.ToArray());
        }

        /// <summary>
        /// Checks if submission is complete
        /// </summary>
        /// <param name="text">User submission</param>
        /// <returns>If submission is complete</returns>
        protected abstract bool IsCompleteSubmission(string text);

        /// <summary>
        /// Evaluates submission
        /// </summary>
        /// <param name="text">User submission</param>
        protected abstract void EvaluateSubmission(string text);

        /// <summary>
        /// Method for handling help command
        /// </summary>
        [MetaCommand("help", "Shows help")]
        protected void EvaluateHelp()
        {
            var maxNameLength = MetaCommands.Max(mc => mc.Name.Length);

            foreach (var metaCommand in MetaCommands.OrderBy(mc => mc.Name))
            {
                var metaParams = metaCommand.Method.GetParameters();
                if (metaParams.Length == 0)
                {
                    var paddedName = metaCommand.Name.PadRight(maxNameLength);

                    Console.Out.WritePunctuation("#");
                    Console.Out.WriteIdentifier(paddedName);
                }
                else
                {
                    Console.Out.WritePunctuation("#");
                    Console.Out.WriteIdentifier(metaCommand.Name);
                    foreach (var pi in metaParams)
                    {
                        Console.Out.WriteSpace();
                        Console.Out.WritePunctuation("<");
                        Console.Out.WriteIdentifier(pi.Name!);
                        Console.Out.WritePunctuation(">");
                    }
                    Console.Out.WriteLine();
                    Console.Out.WriteSpace();
                    for (int _ = 0; _ < maxNameLength; _++)
                        Console.Out.WriteSpace();

                }
                Console.Out.WriteSpace();
                Console.Out.WriteSpace();
                Console.Out.WriteSpace();
                Console.Out.WritePunctuation(metaCommand.Description);
                Console.Out.WriteLine();
            }
        }

        #endregion Methods

    }
}
