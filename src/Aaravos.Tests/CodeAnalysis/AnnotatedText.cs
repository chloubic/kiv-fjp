using Aaravos.Core.CodeAnalysis.Texting;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Text;

namespace Aaravos.Tests.CodeAnalysis
{

    /// <summary>
    /// Handles annotating of text
    /// </summary>
    internal sealed class AnnotatedText
    {

        #region Properties

        /// <summary>
        /// Property for text to annotate
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Text span list property
        /// </summary>
        public ImmutableArray<TextSpan> Spans { get; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Default constructor of <c>AnnotatedText</c>
        /// </summary>
        /// <param name="text">Text to annotate</param>
        /// <param name="spans">Text span list</param>
        public AnnotatedText(
            string text,
            ImmutableArray<TextSpan> spans
        )
        {
            Text = text;
            Spans = spans;
        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Parses annotated text
        /// </summary>
        /// <param name="text">Text line</param>
        /// <returns>Annotated text instance</returns>
        public static AnnotatedText Parse(string text)
        {
            text = Unindent(text);

            var textBuilder = new StringBuilder();
            var spanBuilder = ImmutableArray.CreateBuilder<TextSpan>();
            var startStack = new Stack<int>();

            var position = 0;

            foreach (var c in text)
            {
                if (c == '[')
                {
                    startStack.Push(position);
                }
                else if (c == ']')
                {
                    if (startStack.Count == 0)
                        throw new ArgumentException("Too many ']' in text", nameof(text));

                    var start = startStack.Pop();
                    var end = position;
                    var span = TextSpan.FromBounds(start, end);
                    spanBuilder.Add(span);
                }
                else
                {
                    position++;
                    textBuilder.Append(c);
                }
            }

            if (startStack.Count != 0)
                throw new ArgumentException("Missing ']' in text", nameof(text));

            return new AnnotatedText(textBuilder.ToString(), spanBuilder.ToImmutable());
        }

        /// <summary>
        /// Unindents line by new line character
        /// </summary>
        /// <param name="text">Indented line</param>
        /// <returns>Text separated by new line</returns>
        private static string Unindent(string text)
            => string.Join(Environment.NewLine, UnindentLines(text));

        /// <summary>
        /// Unindents lines
        /// </summary>
        /// <param name="text">Indented line</param>
        /// <returns>Separated line into words</returns>
        public static string[] UnindentLines(string text)
        {
            var lines = new List<string>();

            using (var reader = new StringReader(text))
            {
                string? line;
                while ((line = reader.ReadLine()) != null)
                    lines.Add(line);
            }

            var minIndentation = int.MaxValue;
            for (var i = 0; i < lines.Count; i++)
            {
                var line = lines[i];

                if (line.Trim().Length == 0)
                {
                    lines[i] = string.Empty;
                    continue;
                }

                var indentation = line.Length - line.TrimStart().Length;
                minIndentation = Math.Min(minIndentation, indentation);
            }

            for (var i = 0; i < lines.Count; i++)
            {
                if (lines[i].Length == 0)
                    continue;

                lines[i] = lines[i].Substring(minIndentation);
            }

            while (lines.Count > 0 && lines[0].Length == 0)
                lines.RemoveAt(0);

            while (lines.Count > 0 && lines[lines.Count - 1].Length == 0)
                lines.RemoveAt(lines.Count - 1);

            return lines.ToArray();
        }

        #endregion Methods

    }
}
