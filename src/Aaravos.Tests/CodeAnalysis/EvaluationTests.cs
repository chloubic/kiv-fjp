using Aaravos.Core.CodeAnalysis;
using Aaravos.Core.CodeAnalysis.Analyzing.Symbols;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using Aaravos.Core.CodeAnalysis.Constants;
using Aaravos.Core.CodeAnalysis.Diagnosting;
using System;
using System.Collections.Generic;
using Xunit;

namespace Aaravos.Tests.CodeAnalysis
{

    /// <summary>
    /// Tests for evaluation API
    /// </summary>
    public class EvaluationTests
    {

        #region Theories

        [Theory]
        [InlineData("1", 1)]
        [InlineData("+1", 1)]
        [InlineData("-1", -1)]
        [InlineData("~1", -2)]
        [InlineData("14 + 12", 26)]
        [InlineData("12 - 3", 9)]
        [InlineData("4 * 2", 8)]
        [InlineData("9 / 3", 3)]
        [InlineData("(10)", 10)]
        [InlineData("12 == 3", false)]
        [InlineData("3 == 3", true)]
        [InlineData("12 != 3", true)]
        [InlineData("3 != 3", false)]
        [InlineData("3 < 4", true)]
        [InlineData("5 < 4", false)]
        [InlineData("4 <= 4", true)]
        [InlineData("4 <= 5", true)]
        [InlineData("5 <= 4", false)]
        [InlineData("4 > 3", true)]
        [InlineData("4 > 5", false)]
        [InlineData("4 >= 4", true)]
        [InlineData("5 >= 4", true)]
        [InlineData("4 >= 5", false)]
        [InlineData("1 | 2", 3)]
        [InlineData("1 | 0", 1)]
        [InlineData("1 & 3", 1)]
        [InlineData("1 & 0", 0)]
        [InlineData("1 ^ 0", 1)]
        [InlineData("0 ^ 1", 1)]
        [InlineData("1 ^ 3", 2)]
        [InlineData(KeywordConstants.FALSE + " == " + KeywordConstants.FALSE, true)]
        [InlineData(KeywordConstants.TRUE + " == " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.FALSE + " != " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.TRUE + " != " + KeywordConstants.FALSE, true)]
        [InlineData(KeywordConstants.TRUE + " && " + KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.FALSE + " || " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.FALSE + " | " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.FALSE + " | " + KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.TRUE + " | " + KeywordConstants.FALSE, true)]
        [InlineData(KeywordConstants.TRUE + " | " + KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.FALSE + " & " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.FALSE + " & " + KeywordConstants.TRUE, false)]
        [InlineData(KeywordConstants.TRUE + " & " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.TRUE + " & " + KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.FALSE + " ^ " + KeywordConstants.FALSE, false)]
        [InlineData(KeywordConstants.TRUE + " ^ " + KeywordConstants.FALSE, true)]
        [InlineData(KeywordConstants.FALSE + " ^ " + KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.TRUE + " ^ " + KeywordConstants.TRUE, false)]
        [InlineData(KeywordConstants.TRUE, true)]
        [InlineData(KeywordConstants.FALSE, false)]
        [InlineData("!" + KeywordConstants.TRUE, false)]
        [InlineData("!" + KeywordConstants.FALSE, true)]
        [InlineData(KeywordConstants.VAR + " a = 10 " + KeywordConstants.RETURN + " a", 10)]
        [InlineData("\"test\"", "test")]
        [InlineData("\"te\"\"st\"", "te\"st")]
        [InlineData("\"test\" == \"test\"", true)]
        [InlineData("\"test\" != \"test\"", false)]
        [InlineData("\"test\" == \"abc\"", false)]
        [InlineData("\"test\" != \"abc\"", true)]
        [InlineData("\"test\" + \"abc\"", "testabc")]
        [InlineData("{ " + KeywordConstants.VAR + " a : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.VAR + " b : " + KeywordConstants.TYPE_ANY + " = \"b\" " + KeywordConstants.RETURN + " a == b }", false)]
        [InlineData("{ " + KeywordConstants.VAR + " a : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.VAR + " b : " + KeywordConstants.TYPE_ANY + " = \"b\" " + KeywordConstants.RETURN + " a != b }", true)]
        [InlineData("{ " + KeywordConstants.VAR + " a : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.VAR + " b : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.RETURN + " a == b }", true)]
        [InlineData("{ " + KeywordConstants.VAR + " a : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.VAR + " b : " + KeywordConstants.TYPE_ANY + " = 0 " + KeywordConstants.RETURN + " a != b }", false)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 10 " + KeywordConstants.RETURN + " a * a }", 100)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.RETURN + " (a = 10) * a }", 100)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.IF + " (a == 0) a = 10 " + KeywordConstants.RETURN + " a }", 10)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.IF + " (a == 4) a = 10 " + KeywordConstants.RETURN + " a }", 0)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.IF + " (a == 0) a = 10 " + KeywordConstants.ELSE + " a = 5 " + KeywordConstants.RETURN + " a }", 10)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.IF + " (a == 4) a = 10 " + KeywordConstants.ELSE + " a = 5 " + KeywordConstants.RETURN + " a }", 5)]
        [InlineData("{ " + KeywordConstants.VAR + " i = 10 " + KeywordConstants.VAR + " result = 0 " + KeywordConstants.WHILE + " (i > 0) { result = result + i i = i - 1} " + KeywordConstants.RETURN + " result }", 55)]
        [InlineData("{ " + KeywordConstants.VAR + " result = 0 " + KeywordConstants.FOR + " (i = 1 " + KeywordConstants.TO + " 10) { result = result + i } " + KeywordConstants.RETURN + " result }", 55)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 10 " + KeywordConstants.FOR + " (i = 1 " + KeywordConstants.TO + " (a = a - 1)) { } " + KeywordConstants.RETURN + " a }", 9)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 0 " + KeywordConstants.DO + " a = a + 1 " + KeywordConstants.WHILE + " (a < 10) " + KeywordConstants.RETURN + " a }", 10)]
        [InlineData("{ " + KeywordConstants.VAR + " i = 0 " + KeywordConstants.WHILE + " (i < 5) { i = i + 1 " + KeywordConstants.IF + " (i == 5) " + KeywordConstants.CONTINUE + " } " + KeywordConstants.RETURN + " i }", 5)]
        [InlineData("{ " + KeywordConstants.VAR + " i = 0 " + KeywordConstants.DO + " { i = i + 1 " + KeywordConstants.IF + " (i == 5) " + KeywordConstants.CONTINUE + " } " + KeywordConstants.WHILE + " (i < 5) " + KeywordConstants.RETURN + " i }", 5)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a += (2 + 3) " + KeywordConstants.RETURN + " a }", 6)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a -= (2 + 3) " + KeywordConstants.RETURN + " a }", -4)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a *= (2 + 3) " + KeywordConstants.RETURN + " a }", 5)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a /= (2 + 3) " + KeywordConstants.RETURN + " a }", 0)]
        [InlineData("{ " + KeywordConstants.VAR + " a = " + KeywordConstants.TRUE + " a &= (" + KeywordConstants.FALSE + ") " + KeywordConstants.RETURN + " a }", false)]
        [InlineData("{ " + KeywordConstants.VAR + " a = " + KeywordConstants.TRUE + " a |= (" + KeywordConstants.FALSE + ") " + KeywordConstants.RETURN + " a }", true)]
        [InlineData("{ " + KeywordConstants.VAR + " a = " + KeywordConstants.TRUE + " a ^= (" + KeywordConstants.TRUE + ") " + KeywordConstants.RETURN + " a }", false)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a |= 0 " + KeywordConstants.RETURN + " a }", 1)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a &= 3 " + KeywordConstants.RETURN + " a }", 1)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a &= 0 " + KeywordConstants.RETURN + " a }", 0)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 a ^= 0 " + KeywordConstants.RETURN + " a }", 1)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 " + KeywordConstants.VAR + " b = 2 " + KeywordConstants.VAR + " c = 3 a += b += c " + KeywordConstants.RETURN + " a }", 6)]
        [InlineData("{ " + KeywordConstants.VAR + " a = 1 " + KeywordConstants.VAR + " b = 2 " + KeywordConstants.VAR + " c = 3 a += b += c " + KeywordConstants.RETURN + " b }", 5)]
        public void Evaluator_Computes_CorrectValues(string text, object expectedValue)
        {
            AssertValue(text, expectedValue);
        }

        #endregion Theories

        #region Facts

        [Fact]
        public void Evaluator_VariableDeclaration_Reports_Redeclaration()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" x = 10
                    " + KeywordConstants.VAR + @" y = 100
                    {
                        " + KeywordConstants.VAR + @" x = 10
                    }
                    " + KeywordConstants.VAR + @" [x] = 5
                }
            ";

            var diagnostics = @"
                'x' is already declared.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_BlockStatement_NoInfiniteLoop()
        {
            var text = @"
                {
                [)][]
            ";

            var diagnostics = @"
                Unexpected token <CloseParenthesisToken>, expected <IdentifierToken>.
                Unexpected token <EndOfFileToken>, expected <CloseBraceToken>.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_InvokeFunctionArguments_Missing()
        {
            var text = @"
                " + KeywordConstants.FUNC_PRINT + @"([)]
            ";

            var diagnostics = @"
                Function '" + KeywordConstants.FUNC_PRINT + @"' requires 1 arguments but was given 0.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_InvokeFunctionArguments_Exceeding()
        {
            var text = @"
                " + KeywordConstants.FUNC_PRINT + @"(""Hello""[, "" "", "" world!""])
            ";

            var diagnostics = @"
                Function '" + KeywordConstants.FUNC_PRINT + @"' requires 1 arguments but was given 3.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_InvokeFunctionArguments_NoInfiniteLoop()
        {
            var text = @"
                print(""Hi""[[=]][)]
            ";

            var diagnostics = @"
                Unexpected token <EqualsToken>, expected <CloseParenthesisToken>.
                Unexpected token <EqualsToken>, expected <IdentifierToken>.
                Unexpected token <CloseParenthesisToken>, expected <IdentifierToken>.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_FunctionParameters_NoInfiniteLoop()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" hi(name: string[[[=]]][)]
                {
                    print(""Hi "" + name + ""!"" )
                }[]
            ";

            var diagnostics = @"
                Unexpected token <EqualsToken>, expected <CloseParenthesisToken>.
                Unexpected token <EqualsToken>, expected <OpenBraceToken>.
                Unexpected token <EqualsToken>, expected <IdentifierToken>.
                Unexpected token <CloseParenthesisToken>, expected <IdentifierToken>.
                Unexpected token <EndOfFileToken>, expected <CloseBraceToken>.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_FunctionReturn_Missing()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" [add](a: " + KeywordConstants.TYPE_NUMBER + @", b: " + KeywordConstants.TYPE_NUMBER + @"): " + KeywordConstants.TYPE_NUMBER + @"
                {
                }
            ";

            var diagnostics = @"
                Not all code paths return a value.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_IfStatement_Reports_CannotConvert()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" x = 0
                    " + KeywordConstants.IF + @" ([10])
                        x = 10
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_NUMBER + @"' to '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_WhileStatement_Reports_CannotConvert()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" x = 0
                    " + KeywordConstants.WHILE + @" ([10])
                        x = 10
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_NUMBER + @"' to '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_DoWhileStatement_Reports_CannotConvert()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" x = 0
                    " + KeywordConstants.DO + @"
                        x = 10
                    " + KeywordConstants.WHILE + @" ([10])
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_NUMBER + @"' to '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_ForStatement_Reports_CannotConvert_LowerBound()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" result = 0
                    " + KeywordConstants.FOR + @" (i = [" + KeywordConstants.FALSE + @"] " + KeywordConstants.TO + @" 10)
                        result = result + i
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_BOOL + @"' to '" + KeywordConstants.TYPE_NUMBER + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_ForStatement_Reports_CannotConvert_UpperBound()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" result = 0
                    " + KeywordConstants.FOR + @" (i = 1 " + KeywordConstants.TO + @" [" + KeywordConstants.TRUE + @"])
                        result = result + i
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_BOOL + @"' to '" + KeywordConstants.TYPE_NUMBER + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_NameExpression_Reports_Undefined()
        {
            var text = @"[x] * 10";

            var diagnostics = @"
                Variable 'x' doesn't exist.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_NameExpression_Reports_NoErrorForInsertedToken()
        {
            var text = @"1 + []";

            var diagnostics = @"
                Unexpected token <EndOfFileToken>, expected <IdentifierToken>.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_UnaryExpression_Reports_Undefined()
        {
            var text = @"[+]" + KeywordConstants.TRUE;

            var diagnostics = @"
                Unary operator '+' is not defined for type '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_BinaryExpression_Reports_Undefined()
        {
            var text = @"10 [*] " + KeywordConstants.FALSE;

            var diagnostics = @"
                Binary operator '*' is not defined for types '" + KeywordConstants.TYPE_NUMBER + @"' and '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_CompoundExpression_Reports_Undefined()
        {
            var text = KeywordConstants.VAR + @" x = 10
                         x [+=] " + KeywordConstants.FALSE + @"";

            var diagnostics = @"
                Binary operator '+=' is not defined for types '" + KeywordConstants.TYPE_NUMBER + @"' and '" + KeywordConstants.TYPE_BOOL + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_AssignmentExpression_Reports_Undefined()
        {
            var text = @"[x] = 10";

            var diagnostics = @"
                Variable 'x' doesn't exist.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_CompoundExpression_Assignemnt_NonDefinedVariable_Reports_Undefined()
        {
            var text = @"[x] += 10";

            var diagnostics = @"
                Variable 'x' doesn't exist.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_AssignmentExpression_Reports_NotAVariable()
        {
            var text = @"[" + KeywordConstants.FUNC_PRINT + @"] = 42";

            var diagnostics = @"
                '" + KeywordConstants.FUNC_PRINT + @"' is not a variable.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_AssignmentExpression_Reports_CannotAssign()
        {
            var text = @"
                {
                    " + KeywordConstants.CONST + @" x = 10
                    x [=] 0
                }
            ";

            var diagnostics = @"
                Variable 'x' is read-only and cannot be assigned to.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_CompoundDeclarationExpression_Reports_CannotAssign()
        {
            var text = @"
                {
                    " + KeywordConstants.CONST + @" x = 10
                    x [+=] 1
                }
            ";

            var diagnostics = @"
                Variable 'x' is read-only and cannot be assigned to.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_AssignmentExpression_Reports_CannotConvert()
        {
            var text = @"
                {
                    " + KeywordConstants.VAR + @" x = 10
                    x = [" + KeywordConstants.TRUE + @"]
                }
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_BOOL + @"' to '" + KeywordConstants.TYPE_NUMBER + @"'.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_CallExpression_Reports_Undefined()
        {
            var text = @"[foo](42)";

            var diagnostics = @"
                Function 'foo' doesn't exist.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_CallExpression_Reports_NotAFunction()
        {
            var text = @"
                {
                    " + KeywordConstants.CONST + @" foo = 42
                    [foo](42)
                }
            ";

            var diagnostics = @"
                'foo' is not a function.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Variables_Can_Shadow_Functions()
        {
            var text = @"
                {
                    " + KeywordConstants.CONST + @" " + KeywordConstants.FUNC_PRINT + @" = 42
                    [" + KeywordConstants.FUNC_PRINT + @"](""test"")
                }
            ";

            var diagnostics = @"
                '" + KeywordConstants.FUNC_PRINT + @"' is not a function.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Void_Function_Should_Not_Return_Value()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test()
                {
                    " + KeywordConstants.RETURN + @" [1]
                }
            ";

            var diagnostics = @"
                Since the function 'test' does not return a value the '" + KeywordConstants.RETURN + @"' keyword cannot be followed by an expression.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Function_With_ReturnValue_Should_Not_Return_Void()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test(): " + KeywordConstants.TYPE_NUMBER + @"
                {
                    [" + KeywordConstants.RETURN + @"]
                }
            ";

            var diagnostics = @"
                An expression of type '" + KeywordConstants.TYPE_NUMBER + @"' is expected.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Not_All_Code_Paths_Return_Value()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" [test](n: " + KeywordConstants.TYPE_NUMBER + @"): " + KeywordConstants.TYPE_BOOL + @"
                {
                    " + KeywordConstants.IF + @" (n > 10)
                       " + KeywordConstants.RETURN + @" " + KeywordConstants.TRUE + @"
                }
            ";

            var diagnostics = @"
                Not all code paths return a value.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Expression_Must_Have_Value()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test(n: " + KeywordConstants.TYPE_NUMBER + @")
                {
                    " + KeywordConstants.RETURN + @"
                }

                " + KeywordConstants.CONST + @" value = [test(100)]
            ";

            var diagnostics = @"
                Expression must have a value.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_IfStatement_Reports_NotReachableCode_Warning()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test()
                {
                    " + KeywordConstants.CONST + @" x = 4 * 3
                    " + KeywordConstants.IF + @" (x > 12)
                    {
                        [" + KeywordConstants.FUNC_PRINT + @"](""x"")
                    }
                    " + KeywordConstants.ELSE + @"
                    {
                        " + KeywordConstants.FUNC_PRINT + @"(""x"")
                    }
                }
            ";

            var diagnostics = @"
                Unreachable code detected.
            ";
            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_ElseStatement_Reports_NotReachableCode_Warning()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test(): " + KeywordConstants.TYPE_NUMBER + @"
                {
                    " + KeywordConstants.IF + @" (" + KeywordConstants.TRUE + @")
                    {
                        " + KeywordConstants.RETURN + @" 1
                    }
                    " + KeywordConstants.ELSE + @"
                    {
                        [" + KeywordConstants.RETURN + @"] 0
                    }
                }
            ";

            var diagnostics = @"
                Unreachable code detected.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_WhileStatement_Reports_NotReachableCode_Warning()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test()
                {
                    " + KeywordConstants.WHILE + @" (" + KeywordConstants.FALSE + @")
                    {
                        [" + KeywordConstants.CONTINUE + @"]
                    }
                }
            ";

            var diagnostics = @"
                Unreachable code detected.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Theory]
        [InlineData("[" + KeywordConstants.BREAK + "]", KeywordConstants.BREAK)]
        [InlineData("[" + KeywordConstants.CONTINUE + "]", KeywordConstants.CONTINUE)]
        public void Evaluator_Invalid_Break_Or_Continue(string text, string keyword)
        {
            var diagnostics = $@"
                The keyword '{keyword}' can only be used inside of loops.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Script_Return()
        {
            var text = @"
                " + KeywordConstants.RETURN + @"
            ";

            AssertValue(text, "");
        }

        [Fact]
        public void Evaluator_Parameter_Already_Declared()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" sum(a: " + KeywordConstants.TYPE_NUMBER + @", b: " + KeywordConstants.TYPE_NUMBER + @", [a: " + KeywordConstants.TYPE_NUMBER + @"]): " + KeywordConstants.TYPE_NUMBER + @"
                {
                    " + KeywordConstants.RETURN + @" a + b + c
                }
            ";

            var diagnostics = @"
                A parameter with the name 'a' already exists.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Function_Must_Have_Name()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" [(]a: " + KeywordConstants.TYPE_NUMBER + @", b: " + KeywordConstants.TYPE_NUMBER + @"): " + KeywordConstants.TYPE_NUMBER + @"
                {
                    " + KeywordConstants.RETURN + @" a + b
                }
            ";

            var diagnostics = @"
                Unexpected token <OpenParenthesisToken>, expected <IdentifierToken>.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Wrong_Argument_Type()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test(n: " + KeywordConstants.TYPE_NUMBER + @"): " + KeywordConstants.TYPE_BOOL + @"
                {
                    " + KeywordConstants.RETURN + @" n > 10
                }
                " + KeywordConstants.CONST + @" testValue = ""string""
                test([testValue])
            ";

            var diagnostics = @"
                Cannot convert type '" + KeywordConstants.TYPE_STRING + @"' to '" + KeywordConstants.TYPE_NUMBER + @"'. An explicit conversion exists (are you missing a cast?)
            ";

            AssertDiagnostics(text, diagnostics);
        }

        [Fact]
        public void Evaluator_Bad_Type()
        {
            var text = @"
                " + KeywordConstants.FUNCTION + @" test(n: [invalidtype])
                {
                }
            ";

            var diagnostics = @"
                Type 'invalidtype' doesn't exist.
            ";

            AssertDiagnostics(text, diagnostics);
        }

        #endregion Facts

        #region Assertions

        private static void AssertValue(string text, object expectedValue)
        {
            var syntaxTree = SyntaxTree.Parse(text);
            var compilation = Compilation.CreateScript(null, syntaxTree);
            var variables = new Dictionary<VariableSymbol, object>();
            var result = compilation.Evaluate(variables);

            Assert.False(result.Diagnostics.HasErrors());
            Assert.Equal(expectedValue, result.Value);
        }

        private void AssertDiagnostics(string text, string diagnosticText)
        {
            var annotatedText = AnnotatedText.Parse(text);
            var syntaxTree = SyntaxTree.Parse(annotatedText.Text);
            var compilation = Compilation.CreateScript(null, syntaxTree);
            var result = compilation.Evaluate(new Dictionary<VariableSymbol, object>());

            var expectedDiagnostics = AnnotatedText.UnindentLines(diagnosticText);

            if (annotatedText.Spans.Length != expectedDiagnostics.Length)
                throw new Exception("ERROR: Must mark as many spans as there are expected diagnostics");

            var diagnostics = result.Diagnostics;
            Assert.Equal(expectedDiagnostics.Length, diagnostics.Length);

            for (var i = 0; i < expectedDiagnostics.Length; i++)
            {
                var expectedMessage = expectedDiagnostics[i];
                var actualMessage = diagnostics[i].Message;
                Assert.Equal(expectedMessage, actualMessage);

                var expectedSpan = annotatedText.Spans[i];
                var actualSpan = diagnostics[i].Location.Span;
                Assert.Equal(expectedSpan, actualSpan);
            }
        }

        #endregion Assertions

    }
}
