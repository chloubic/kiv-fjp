using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Aaravos.Tests.CodeAnalysis.Syntax
{
    internal sealed class AssertingEnumerator : IDisposable
    {

        #region Properties

        private readonly IEnumerator<SyntaxNode> Enumerator;
        private bool HasErrors;

        #endregion Properties

        #region Constructor

        public AssertingEnumerator(
            SyntaxNode node
        )
        {
            Enumerator = Flatten(node).GetEnumerator();
        }

        #endregion Constructor

        #region Methods

        private bool MarkFailed()
        {
            HasErrors = true;
            return false;
        }

        public void Dispose()
        {
            if (!HasErrors)
                Assert.False(Enumerator.MoveNext());

            Enumerator.Dispose();
        }

        private static IEnumerable<SyntaxNode> Flatten(SyntaxNode node)
        {
            var stack = new Stack<SyntaxNode>();
            stack.Push(node);

            while (stack.Count > 0)
            {
                var n = stack.Pop();
                yield return n;

                foreach (var child in n.GetChildren().Reverse())
                    stack.Push(child);
            }
        }

        #endregion Methods

        #region Assertions

        public void AssertNode(SyntaxKind kind)
        {
            try
            {
                Assert.True(Enumerator.MoveNext());
                Assert.Equal(kind, Enumerator.Current.Kind);
                Assert.IsNotType<SyntaxToken>(Enumerator.Current);
            }
            catch when (MarkFailed())
            {
                throw;
            }
        }

        public void AssertToken(SyntaxKind kind, string text)
        {
            try
            {
                Assert.True(Enumerator.MoveNext());
                Assert.Equal(kind, Enumerator.Current.Kind);
                var token = Assert.IsType<SyntaxToken>(Enumerator.Current);
                Assert.Equal(text, token.Text);
            }
            catch when (MarkFailed())
            {
                throw;
            }
        }

        #endregion Assertions

    }
}
