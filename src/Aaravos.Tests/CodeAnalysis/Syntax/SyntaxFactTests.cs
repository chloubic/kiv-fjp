using Aaravos.Core.CodeAnalysis.Analyzing.Facts;
using Aaravos.Core.CodeAnalysis.Analyzing.Kinds;
using Aaravos.Core.CodeAnalysis.Analyzing.Syntax;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Aaravos.Tests.CodeAnalysis.Syntax
{
    public class SyntaxFactTests
    {

        #region Theories

        [Theory]
        [MemberData(nameof(GetSyntaxKindData))]
        public void SyntaxFact_GetText_RoundTrips(SyntaxKind kind)
        {
            var text = SyntaxFacts.GetText(kind);
            if (text == null)
                return;

            var tokens = SyntaxTree.ParseTokens(text);
            var token = Assert.Single(tokens);
            Assert.Equal(kind, token.Kind);
            Assert.Equal(text, token.Text);
        }

        #endregion Theories

        #region Methods

        public static IEnumerable<object[]> GetSyntaxKindData()
        {
            var kinds = (SyntaxKind[])Enum.GetValues(typeof(SyntaxKind));
            foreach (var kind in kinds)
                yield return new object[] { kind };
        }

        #endregion Methods

    }
}
