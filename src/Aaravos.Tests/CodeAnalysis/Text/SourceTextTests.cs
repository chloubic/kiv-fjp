using Aaravos.Core.CodeAnalysis.Texting;
using Xunit;

namespace Aaravos.Tests.CodeAnalysis.Text
{

    /// <summary>
    /// <c>Source</c> tests
    /// </summary>
    public class SourceTextTests
    {

        #region Theories

        [Theory]
        [InlineData(".", 1)]
        [InlineData(".\r\n", 2)]
        [InlineData(".\r\n\r\n", 3)]
        public void SourceText_IncludesLastLine(string text, int expectedLineCount)
            => Assert.Equal(expectedLineCount, SourceText.From(text).Lines.Length);

        #endregion Theories

    }
}
